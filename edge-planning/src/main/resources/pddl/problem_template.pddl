(define (problem smart-waste1)
  (:domain smart-waste)
  (:objects
   %1$s
  )
  (:init
    %2$s
  )
  (:goal
    %3$s
  )
  (:metric minimize (total-cost))
)
