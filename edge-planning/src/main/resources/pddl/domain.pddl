(define (domain smart-waste)
    (:requirements :fluents :typing :universal-preconditions)
    (:types
    vehicle location street facility - object
    wastebin - location)
    (:predicates  (truck ?t - vehicle)
                  (bin ?b - wastebin)
                  (node ?n - location)
                  (street ?s - street)
                  (plant ?p - facility)
                  (is-at ?p ?n - location)
                  (connected ?s - street ?n - location)
                  (truck-at ?t - vehicle ?n - location)
                  (plant-at ?p - facility ?n - location)
                  (emptied ?b - wastebin)
                  (empty ?t - vehicle)
                  )
    (:functions
     (total-cost)
     (distance ?s - street)
     (capacity ?t - vehicle)
     (wastebin-status ?b - wastebin)
     (wastebin-minimum)
     (wastebin-capacity)
     (max-capacity ?t - vehicle)
    )
    (:action move
     :parameters (?t - vehicle ?from-node - location  ?to-node - location ?s - street)
     :precondition (and (truck ?t) (truck-at ?t ?from-node) (connected ?s ?from-node) (connected ?s ?to-node) )
     :effect (and (truck-at ?t ?to-node) (not (truck-at ?t ?from-node)) (increase (total-cost) (distance ?s)))
    )
    (:action empty-bin
     :parameters (?t - vehicle ?b - wastebin ?n - location)
     :precondition (and (truck-at ?t ?n) (is-at ?b ?n) (>= (wastebin-status ?b) (wastebin-minimum)) (>= (capacity ?t) (wastebin-capacity)))
     :effect (and  (decrease (capacity ?t) (wastebin-status ?b)) (assign (wastebin-status ?b) 0) (emptied ?b) (not (empty ?t)))
    )

    (:action empty-truck
     :parameters (?t - vehicle ?p - facility ?n - location)
     :precondition (and (plant ?p) (plant-at ?p ?n) (truck-at ?t ?n))
     :effect (and (empty ?t) (assign (capacity ?t) (max-capacity ?t)))
    )

)
