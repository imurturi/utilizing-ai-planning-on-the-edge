package util;

import org.opengis.filter.identity.FeatureId;

public class NamingUtils {

	private static int nodeIdSeparatorPosition = 1;
	private static int streetIdSeparatorPosition = 4;

	public static String convertNodeId(Double nodeId){
		if(nodeId != null){
			nodeIdSeparatorPosition = String.valueOf(nodeId).indexOf(".");
			return "n"+String.valueOf(nodeId).replace(".", "");
		}


		return null;
	}

	public static String convertStreetId(FeatureId streetId){
		if(streetId != null){
			streetIdSeparatorPosition = String.valueOf(streetId).indexOf(".");
			return String.valueOf(streetId).replace(".", "");
		}
		return null;
	}

	public static Double fromNodeIDString(String nodeID){
		if(nodeID != null && !nodeID.isEmpty()){
			String transformedNodeID = new String(nodeID);
			if(nodeID.startsWith("n"))
				transformedNodeID = transformedNodeID.substring(1);
			String firstPart = transformedNodeID.substring(0,nodeIdSeparatorPosition);
			String secondPart = transformedNodeID.substring(nodeIdSeparatorPosition);
			transformedNodeID = firstPart+"."+secondPart;

			return Double.valueOf(transformedNodeID);

		}

		return null;
	}

	public static String fromStreedIDString(String streetID){
		String firstPart = streetID.substring(0,streetIdSeparatorPosition);
		String secondPart = streetID.substring(streetIdSeparatorPosition);

		return firstPart+"."+secondPart;
	}
}
