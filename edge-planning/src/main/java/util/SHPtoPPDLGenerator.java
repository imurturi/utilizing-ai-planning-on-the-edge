package util;

import models.*;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.opengis.feature.Attribute;
import org.opengis.feature.simple.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class SHPtoPPDLGenerator {

	public GeneratedCity generateCityPPDL(SimpleFeatureCollection streetFeatureCollection, SimpleFeatureCollection nodeFeatureCollection) {
		GeneratedCity generatedCity = null;
		List<Double> nodeIds = new ArrayList<Double>();
		List<StreetFeature> streets = new ArrayList<StreetFeature>();

		HashMap<String, String> streetNameMap = new HashMap<>();
		HashMap<Double, String> nodeNameMap = new HashMap<>();

		File cityPDDLFile = new File(System.getProperty("java.io.tmpdir")+File.separator+"problem_skeleton" + System.currentTimeMillis() + ".pddl");

		try {
			String templateString = new String(Files.readAllBytes(new File("pddl"+File.separator+"problem_template.pddl").toPath()));

			System.err.println("Generating City with " + streetFeatureCollection.size() + " streets and " + nodeFeatureCollection.size() + " nodes.");

			SimpleFeatureIterator nodeIterator = nodeFeatureCollection.features();
			while (nodeIterator.hasNext()) {
				SimpleFeature node = nodeIterator.next();
				Double nodeId = (Double) node.getAttribute("GIP_OBJECT");
				if (nodeId != null && node.getAttribute("FEATURENAM") != null) {
					String nodeName = new String(((String) node.getAttribute("FEATURENAM")).getBytes(), Charset.forName("UTF-8"));
					nodeNameMap.put(nodeId, nodeName);
				}

				nodeIds.add(nodeId);
			}

			nodeIterator.close();

			SimpleFeatureIterator streetIterator = streetFeatureCollection.features();
			while (streetIterator.hasNext()) {
				SimpleFeature feature = streetIterator.next();

				StreetFeature streetFeature = new StreetFeature(feature.getIdentifier());

				Double distance = (Double) ((Attribute) feature.getProperties().toArray()[16]).getValue();
				Double firstConnectedNodeId = (Double) ((Attribute) feature.getProperties().toArray()[13]).getValue();
				Double secondConnectedNodeId = (Double) ((Attribute) feature.getProperties().toArray()[14]).getValue();

				String streetName = (String) feature.getAttribute("FEATURENAM");
				streetNameMap.put(feature.getID(), streetName);

				streetFeature.setDistance(distance);
				streetFeature.setConnectedIds(new Double[]{firstConnectedNodeId, secondConnectedNodeId});
				if (nodeIds.contains(firstConnectedNodeId) && nodeIds.contains(secondConnectedNodeId))
					streets.add(streetFeature);
			}

			streetIterator.close();

			nodeIds = nodeIds.parallelStream().filter(aDouble -> streets.parallelStream().anyMatch(streetFeature -> (streetFeature.getConnectedIds()[0].equals(aDouble) || streetFeature.getConnectedIds()[1].equals(aDouble)))).collect(Collectors.toList());

			StringBuffer objectAssignment = new StringBuffer();
			final StringBuffer objectDefinition = new StringBuffer();


			nodeIds.forEach(nodeId -> {
				objectDefinition.append(NamingUtils.convertNodeId(nodeId)).append(" - location \n");
				objectAssignment.append("(node ").append(NamingUtils.convertNodeId(nodeId)).append(") \n");
			});

			streets.forEach(streetFeature -> {
				objectDefinition.append(NamingUtils.convertStreetId(streetFeature.getId())).append(" - street \n");
				objectAssignment.append("(street ").append(NamingUtils.convertStreetId(streetFeature.getId())).append(") \n");
				objectAssignment.append("(connected ").append(NamingUtils.convertStreetId(streetFeature.getId())).append(" ").append(NamingUtils.convertNodeId(streetFeature.getConnectedIds()[0])).append(") \n");
				objectAssignment.append("(connected ").append(NamingUtils.convertStreetId(streetFeature.getId())).append(" ").append(NamingUtils.convertNodeId(streetFeature.getConnectedIds()[1])).append(") \n");
				objectAssignment.append("(= (distance ").append(NamingUtils.convertStreetId(streetFeature.getId())).append(") ").append(streetFeature.getDistance()).append(") \n");
			});

			objectDefinition.append("%1$s");
			objectAssignment.append("%2$s");

			String goal = "%3$s";

			String problem = String.format(templateString, objectDefinition, objectAssignment, goal);
			Files.write(cityPDDLFile.toPath(), problem.getBytes());

			generatedCity = new GeneratedCity(cityPDDLFile, nodeIds);
			generatedCity.setStreetNameMap(streetNameMap);
			generatedCity.setNodeNameMap(nodeNameMap);
			generatedCity.setNodesInAuthority(nodeFeatureCollection);
			return generatedCity;


		} catch (IOException e) {
			e.printStackTrace();
			return generatedCity;
		}
	}

	public GeneratedCity generateCityPPDL(SimpleFeatureSource streetFeatureSource, SimpleFeatureSource nodeFeatureSource) {
		try {
			return generateCityPPDL(streetFeatureSource.getFeatures(), nodeFeatureSource.getFeatures());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}


	public String generateEmptyingProblem(EdgeDevice edgeDevice, List<Vehicle> vehicles, int wastebinCapacity, int vehicleCapacity, int wasteBinMinimum) {
		String filePath = System.getProperty("java.io.tmpdir")+File.separator+"problem_" + edgeDevice.getSimulationID() + System.currentTimeMillis() + ".pddl";
		String templateString = null;
		GeneratedCity generatedCity = edgeDevice.getGeneratedCity();
		LinkedList<WasteBin> bins = edgeDevice.getWasteBinsNotInPlanning(wasteBinMinimum);
		ArrayList<Plant> plants = edgeDevice.getPlants();

		if(generatedCity != null && vehicles != null && !vehicles.isEmpty() && bins != null && !bins.isEmpty() && plants != null && !plants.isEmpty()) {
			try {
				templateString = new String(Files.readAllBytes(generatedCity.getGeneratedCityPPDL().toPath()));

				StringBuffer objectAssignment = new StringBuffer();
				final StringBuffer objectDefinition = new StringBuffer();

				for (int i = 0; i < bins.size(); i++) {
					WasteBin bin = bins.get(i);
					objectDefinition.append(bin.getId()).append(" - wastebin \n");
					objectAssignment.append("(bin ").append(bin.getId()).append(") \n");
					objectAssignment.append("(is-at ").append(bin.getId()).append(" ").append(NamingUtils.convertNodeId(bin.getAtNode())).append(") \n");
					objectAssignment.append("(= (wastebin-status ").append(bin.getId()).append(") ").append(Math.round(bin.getFullness() * wastebinCapacity / 100)).append(") \n");
				}

				plants.forEach(plant -> {
					objectDefinition.append(plant.getId()).append(" - facility \n");
					objectAssignment.append("(plant ").append(plant.getId()).append(") \n");
					objectAssignment.append("(plant-at ").append(plant.getId()).append(" ").append(NamingUtils.convertNodeId(plant.getAtNodeId())).append(") \n");
				});

				vehicles.forEach(truck -> {
					objectDefinition.append(truck.getId()).append(" - vehicle \n");
					objectAssignment.append("(truck ").append(truck.getId()).append(") \n");
					objectAssignment.append("(truck-at ").append(truck.getId()).append(" ").append(NamingUtils.convertNodeId(truck.getAtNodeId())).append(") \n");
					objectAssignment.append("(= (capacity ").append(truck.getId()).append(") ").append(truck.getCapacity()).append(") \n");
					objectAssignment.append("(= (max-capacity ").append(truck.getId()).append(") ").append(vehicleCapacity).append(") \n");
				});

				objectAssignment.append("(= (wastebin-capacity) ").append(wastebinCapacity).append(") \n");
				objectAssignment.append("(= (wastebin-minimum) ").append(Math.round(wasteBinMinimum * wastebinCapacity / 100)).append(") \n");

				String goal = "(and\n" +
						"      (forall\n" +
						"        (?b - wastebin) (< (wastebin-status ?b) (wastebin-minimum))\n" +
						"      )\n" +
						"    )";


				String problem = String.format(templateString, objectDefinition, objectAssignment, goal);
				Files.write(Paths.get(filePath), problem.getBytes());


				return filePath;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}



	public String generateMovingProblem(Vehicle vehicleToMove, GeneratedCity generatedCity, Double targetNode) {
		String filePath = System.getProperty("java.io.tmpdir")+File.separator+"problem_" + targetNode + "_" + vehicleToMove.getId() + System.currentTimeMillis() + ".pddl";
		String templateString = null;

		if(!generatedCity.getNodeIds().contains(vehicleToMove.getAtNodeId())){
			return null;
		}

		try {
			templateString = new String(Files.readAllBytes(generatedCity.getGeneratedCityPPDL().toPath()));

			StringBuffer objectAssignment = new StringBuffer();
			final StringBuffer objectDefinition = new StringBuffer();

			objectDefinition.append(vehicleToMove.getId()).append(" - vehicle \n");
			objectAssignment.append("(truck ").append(vehicleToMove.getId()).append(") \n");
			objectAssignment.append("(truck-at ").append(vehicleToMove.getId()).append(" ").append(NamingUtils.convertNodeId(vehicleToMove.getAtNodeId())).append(") \n");

			String goal = "(truck-at " + vehicleToMove.getId() + " " + NamingUtils.convertNodeId(targetNode)+ ")";

			String problem = String.format(templateString, objectDefinition, objectAssignment, goal);
			Files.write(Paths.get(filePath), problem.getBytes());


			return filePath;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
