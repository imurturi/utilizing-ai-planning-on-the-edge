package util;

import models.EdgeDevice;
import models.Vehicle;
import models.WasteBin;

import java.util.ArrayList;
import java.util.LinkedList;

public class ParameterStore {
	private static ParameterStore instance;

	private int binCount = 8;
	private int binCapacity = 10;
	private int vehicleCapacity = 100;
	private int simulationSpeed = 5000;
	private int authorityRadius = 500;
	private int minFullness = 80;

	private int animationSpeed;
	private float simulationStepSpeed;

	private boolean simulationMode = false;


	private ArrayList<EdgeDevice> edgeDevices;
	private ArrayList<Vehicle> unassignedVehicles;

	private ArrayList<Vehicle> lockedVehicles;

	public int getBinCount() {
		return binCount;
	}

	public void setBinCount(int binCount) {
		this.binCount = binCount;
	}

	public int getBinCapacity() {
		return binCapacity;
	}

	public void setBinCapacity(int binCapacity) {
		this.binCapacity = binCapacity;
	}

	public int getVehicleCapacity() {
		return vehicleCapacity;
	}

	public void setVehicleCapacity(int vehicleCapacity) {
		this.vehicleCapacity = vehicleCapacity;
	}

	public int getSimulationSpeed() {
		return simulationSpeed;
	}

	public void setSimulationSpeed(int simulationSpeed) {
		this.simulationSpeed = simulationSpeed;
	}

	public int getAuthorityRadius() {
		return authorityRadius;
	}

	public void setAuthorityRadius(int authorityRadius) {
		this.authorityRadius = authorityRadius;
	}

	public ArrayList<Vehicle> getAllVehicleList() {
		ArrayList<Vehicle> vehicleList = new ArrayList<>();
		if (edgeDevices != null) {
			synchronized (edgeDevices) {
				edgeDevices.forEach(edgeDevice -> {
					if (edgeDevice.getLeasedVehicles() != null)
						vehicleList.addAll(edgeDevice.getLeasedVehicles());
				});
				if (unassignedVehicles != null)
					vehicleList.addAll(unassignedVehicles);
				return vehicleList;
			}
		} else {
			if (unassignedVehicles != null)
				return unassignedVehicles;
			return null;
		}
	}

	public LinkedList<WasteBin> getBinList() {
		LinkedList<WasteBin> binList = new LinkedList<>();

		if (edgeDevices != null) {
			synchronized (edgeDevices) {
				edgeDevices.forEach(edgeDevice -> {
					if (edgeDevice.getWasteBins() != null)
						binList.addAll(edgeDevice.getWasteBins());
				});
				return binList;
			}

		} else {
			return null;
		}
	}

	public ArrayList<Vehicle> getLockedVehicles() {
		if (lockedVehicles == null)
			lockedVehicles = new ArrayList<>();
		return lockedVehicles;
	}

	public void setLockedVehicles(ArrayList<Vehicle> lockedVehicles) {
		this.lockedVehicles = lockedVehicles;
	}

	public ArrayList<Vehicle> getUnassignedVehicles() {
		if (unassignedVehicles == null)
			unassignedVehicles = new ArrayList<>();
		return unassignedVehicles;
	}

	public void setUnassignedVehicles(ArrayList<Vehicle> unassignedVehicles) {
		this.unassignedVehicles = unassignedVehicles;
	}

	public synchronized ArrayList<EdgeDevice> getEdgeDevices() {
		return edgeDevices;
	}

	public synchronized void setEdgeDevices(ArrayList<EdgeDevice> edgeDevices) {
		this.edgeDevices = edgeDevices;
	}

	public float getSimulationStepSpeed() {
		return simulationStepSpeed;
	}

	public void setSimulationStepSpeed(float simulationStepSpeed) {
		this.simulationStepSpeed = simulationStepSpeed;
	}

	public static ParameterStore getInstance() {
		if (instance == null)
			instance = new ParameterStore();
		return instance;
	}

	public int getAnimationSpeed() {
		return animationSpeed;
	}

	public void setAnimationSpeed(int animationSpeed) {
		this.animationSpeed = animationSpeed;
	}

	public void setMinWasteBinFullness(int minFullness) {
		this.minFullness = minFullness;
	}

	public int getMinFullness() {
		return minFullness;
	}

	public boolean isSimulationMode() {
		return simulationMode;
	}

	public void setSimulationMode(boolean simulationMode) {
		this.simulationMode = simulationMode;
	}
}
