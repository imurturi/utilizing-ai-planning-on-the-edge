package util;

import models.EdgeDevice;
import models.Vehicle;
import models.WasteBin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UseCaseObserver {
    private static UseCaseObserver instance = new UseCaseObserver();

    private static Logger LOG = LogManager.getLogger("UsecaseLogger");
    private Integer totalWasteBinFullTime = 0;
    private Integer totalPlansGenerated = 0;

    public UseCaseObserver() {
    }

    public void initUsecase1() {
        useCaseEdgeNodeIds = new ArrayList<>();
        useCaseEdgeNodeIds.add(Double.valueOf("1.0349895E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.8836986E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0352331E7"));


        useCaseVehicleNodeIds = new ArrayList<>();
        useCaseVehicleNodeIds.add(Double.valueOf("1.035235E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0352389E7"));
    }

    public void initUsecase2() {
        useCaseEdgeNodeIds = new ArrayList<>();
        useCaseEdgeNodeIds.add(Double.valueOf("1.0349766E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0349895E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0352191E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.8836986E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0352331E7"));


        useCaseVehicleNodeIds = new ArrayList<>();
        useCaseVehicleNodeIds.add(Double.valueOf("1.0349746E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.035235E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0352389E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("2.0018501E7"));
    }

    public void initUsecase3() {
        useCaseEdgeNodeIds = new ArrayList<>();
        useCaseEdgeNodeIds.add(Double.valueOf("1.0351314E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0351124E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.034984E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0349724E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0347857E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0348242E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("2.0052844E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0347926E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0347889E7"));
        useCaseEdgeNodeIds.add(Double.valueOf("1.0346153E7"));


        useCaseVehicleNodeIds = new ArrayList<>();
        useCaseVehicleNodeIds.add(Double.valueOf("1.0346531E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0346135E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0349723E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0351183E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0351227E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0349992E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0348066E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0346217E7"));
    }

    public void initPerfTest1() {
        useCaseEdgeNodeIds = new ArrayList<>();
        useCaseEdgeNodeIds.add(Double.valueOf("1.0351323E7"));


        useCaseVehicleNodeIds = new ArrayList<>();
        useCaseVehicleNodeIds.add(Double.valueOf("1.0351303E7"));
    }

    public void initPerfTest2() {
        useCaseEdgeNodeIds = new ArrayList<>();
        useCaseEdgeNodeIds.add(Double.valueOf("1.0351323E7"));


        useCaseVehicleNodeIds = new ArrayList<>();
        useCaseVehicleNodeIds.add(Double.valueOf("1.0351303E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0352335E7"));
    }

    public void initPerfTest3() {
        useCaseEdgeNodeIds = new ArrayList<>();
        useCaseEdgeNodeIds.add(Double.valueOf("1.0351323E7"));


        useCaseVehicleNodeIds = new ArrayList<>();
        useCaseVehicleNodeIds.add(Double.valueOf("1.0351303E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0352335E7"));
        useCaseVehicleNodeIds.add(Double.valueOf("1.0352368E7"));

    }

    private float totalCost = 0;
    private float totalPlanningTime = 0;
    private float totalPlanExecutionTime = 0;

    private float totalDaysElapsed = 0;

    private HashMap<WasteBin, Integer> timeWasteBinFull = new HashMap<>();
    private HashMap<EdgeDevice, Integer> timeEdgeDeviceFull = new HashMap<>();
    private HashMap<EdgeDevice, Integer> binsEmptied = new HashMap<>();
    private HashMap<Vehicle, Integer> vehicleErrorModeCount = new HashMap<>();

    private List<Double> useCaseEdgeNodeIds;
    private List<Double> useCaseVehicleNodeIds;

    public void increaseTotalCost(float increaseBy) {
        totalCost += increaseBy;
    }

    public void increaseTotalPlanExectionTime(float increaseBy) {
        totalPlanExecutionTime += increaseBy;
    }

    public void increaseTotalDaysElapsed(float increaseBy) {
        totalDaysElapsed += increaseBy;
    }

    public void increasePlansGenerated() {
        totalPlansGenerated += 1;
    }

    public void increaseVehicleErrorCount(Vehicle vehicle) {
        if (vehicleErrorModeCount.containsKey(vehicle)) {
            vehicleErrorModeCount.put(vehicle, vehicleErrorModeCount.get(vehicle) + 1);
        } else {
            vehicleErrorModeCount.put(vehicle, 1);
        }
    }

    public void increaseTimeWasteBinFull(EdgeDevice edgeDevice, WasteBin wasteBin, Integer timeElapsed) {
        synchronized (timeWasteBinFull) {
            if (timeWasteBinFull.containsKey(wasteBin)) {
                totalWasteBinFullTime += timeElapsed;
                Integer prevTimeElapsed = timeWasteBinFull.get(wasteBin);
                timeWasteBinFull.put(wasteBin, (prevTimeElapsed + timeElapsed));

                Integer prevTotalTimeElapsed = timeEdgeDeviceFull.get(edgeDevice);
                timeEdgeDeviceFull.put(edgeDevice, (prevTotalTimeElapsed + timeElapsed));
            } else {
                timeWasteBinFull.put(wasteBin, timeElapsed);
                timeEdgeDeviceFull.put(edgeDevice, timeElapsed);
            }
        }
    }

    public float getTotalCost() {
        return totalCost;
    }

    public static UseCaseObserver getInstance() {
        return instance;
    }

    public int getBinsEmptied() {
        int totalBinsEmptied = 0;
        for (Integer value : binsEmptied.values()) {
            totalBinsEmptied += value;
        }

        return totalBinsEmptied;
    }

    public float getAverageFullTime() {
        float totalFullTime = 0;
        synchronized (timeEdgeDeviceFull) {
            for (Integer value : timeEdgeDeviceFull.values()) {
                totalFullTime += value;
            }
            totalFullTime /= timeEdgeDeviceFull.size();
        }

        return totalFullTime;
    }

    public void printLogs() {
        if (timeWasteBinFull != null && timeEdgeDeviceFull != null && binsEmptied != null && vehicleErrorModeCount != null) {
            Double averageFullTime = Double.valueOf(totalWasteBinFullTime) / timeWasteBinFull.keySet().size();
            boolean simulationMode = ParameterStore.getInstance().isSimulationMode();
            int totalWasteBinsEmptied = 0;


            for (EdgeDevice edgeDevice : timeEdgeDeviceFull.keySet()) {
                LOG.info("Waste Bins for Edge Device {} have been full for a total of {} hours", edgeDevice.getSimulationID(), timeEdgeDeviceFull.get(edgeDevice));
                double edgeDeviceAverageFullTime = timeEdgeDeviceFull.get(edgeDevice);
                int numWasteBinsFull = 0;

                for (WasteBin wasteBin : edgeDevice.getWasteBins()) {
                    if (timeWasteBinFull.containsKey(wasteBin)) {
                        numWasteBinsFull += 1;
                        LOG.info("      Waste Bin {} for Edge Device {} has been full for {} hours", wasteBin.getId(), edgeDevice.getSimulationID(), timeWasteBinFull.get(wasteBin));
                    }
                }

                if (numWasteBinsFull != 0) {
                    edgeDeviceAverageFullTime /= numWasteBinsFull;
                    LOG.info("Waste Bins for Edge Device {} have been full on average of {} hours", edgeDevice.getSimulationID(), edgeDeviceAverageFullTime);

                }

                LOG.info("Total Bins Emptied for {}: {}", edgeDevice.getSimulationID(), binsEmptied.get(edgeDevice));
                totalWasteBinsEmptied += binsEmptied.get(edgeDevice);
                LOG.info("---");
            }

            float totalPlanExectionTimeInH = totalPlanExecutionTime / 60;
            if (simulationMode) {
                totalPlanExectionTimeInH = totalPlanExectionTimeInH / 60;
            }
            float averageSpeed = (totalCost / 1000) / ((totalPlanExectionTimeInH));

            LOG.info("Total Days Elapsed: {} days", totalDaysElapsed);
            LOG.info("Total Plan Cost: {}", totalCost);
            LOG.info("Total Plans Generated: {}", totalPlansGenerated);
            LOG.info("Total Bins Emptied: {}", getBinsEmptied());
            LOG.info("Total Plan Execution Time: {} h", totalPlanExectionTimeInH);
            LOG.info("Total Planning Time: {} s", totalPlanningTime / 1000);
            LOG.info("Average Full Time: {} h", averageFullTime);
            LOG.info("Average Vehicle Speed: {} km/h", averageSpeed);

            LOG.info("ERRORS:");
            vehicleErrorModeCount.keySet().forEach(vehicle -> LOG.info("Vehicle {} had {} errors.", vehicle, vehicleErrorModeCount.get(vehicle)));
        }
    }

    public void increasePlanningTime(float planningTime) {
        this.totalPlanningTime += planningTime;
    }

    public void addBinEmptied(EdgeDevice edgeDevice) {
        if (binsEmptied.containsKey(edgeDevice)) {
            Integer binsEmptiedCount = binsEmptied.get(edgeDevice);
            binsEmptied.put(edgeDevice, binsEmptiedCount + 1);
        } else {
            binsEmptied.put(edgeDevice, 1);
        }
    }

    public List<Double> getUseCaseEdgeNodeIds() {
        return useCaseEdgeNodeIds;
    }

    public List<Double> getUseCaseVehicleNodeIds() {
        return useCaseVehicleNodeIds;
    }

    public void resetStats() {
        timeWasteBinFull.clear();
        timeEdgeDeviceFull.clear();
        totalPlansGenerated = 0;
        totalPlanningTime = 0;
        totalDaysElapsed = 0;
        totalPlanExecutionTime = 0;
        totalCost = 0;
        totalWasteBinFullTime = 0;
        binsEmptied.clear();
        vehicleErrorModeCount.clear();
    }


}
