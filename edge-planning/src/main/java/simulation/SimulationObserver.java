package simulation;

import gui.drawers.GraphicDrawCoordinator;
import models.WasteBin;
import util.ParameterStore;

import java.util.LinkedList;

public class SimulationObserver {

	private GraphicDrawCoordinator drawCoordinator;

	public SimulationObserver(GraphicDrawCoordinator drawCoordinator) {
		this.drawCoordinator = drawCoordinator;
	}

}
