package simulation;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SimulationConnector {

	private String simulationBaseURL;

	public SimulationConnector(String simulationBaseURL) {
		this.simulationBaseURL = simulationBaseURL;
	}

	public SimulationStatus getSimulationStatus(){
		try {
			URL simulationURL = new URL(simulationBaseURL+"/status");
			HttpURLConnection httpURLConnection = (HttpURLConnection) simulationURL.openConnection();
			httpURLConnection.setRequestMethod("GET");
			InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
			BufferedReader json = new BufferedReader(inputStreamReader);
			Gson gson = new Gson();
			SimulationStatus simulationStatus = gson.fromJson(json, SimulationStatus.class);
			inputStreamReader.close();
			httpURLConnection.disconnect();
			return simulationStatus;
		} catch (IOException e) {
			System.out.println("Simulation Server is not running!");
			return null;
		}
	}

	public void emptyBin(int binIndex){
		try {
			URL simulationURL = new URL(simulationBaseURL+"/empty?bin="+binIndex);
			HttpURLConnection httpURLConnection = (HttpURLConnection) simulationURL.openConnection();
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.getInputStream();
			httpURLConnection.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
