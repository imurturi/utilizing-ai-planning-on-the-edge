package simulation;

import java.util.Arrays;

public class SimulationStatus {
    private int num_bins;
    private float[] bin_fullness;
    private int current_hour;
    private int current_day;

    public int getNum_bins() {
        return num_bins;
    }

    public void setNum_bins(int num_bins) {
        this.num_bins = num_bins;
    }

    public float[] getBin_fullness() {
        return bin_fullness;
    }

    public void setBin_fullness(float[] bin_fullness) {
        this.bin_fullness = bin_fullness;
    }

    public int getCurrent_hour() {
        return current_hour;
    }

    public int getCurrent_day() {
        return current_day;
    }

    @Override
    public String toString() {
        return "SimulationStatus{" +
                "num_bins=" + num_bins +
                ", bin_fullness=" + Arrays.toString(bin_fullness) +
                ", current_day=" +current_day +
                ", current_hour="+current_hour +
                '}';
    }
}
