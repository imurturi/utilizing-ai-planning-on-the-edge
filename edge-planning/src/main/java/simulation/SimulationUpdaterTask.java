package simulation;

import gui.drawers.GraphicDrawCoordinator;
import models.EdgeDevice;
import util.ParameterStore;

import java.util.ArrayList;
import java.util.TimerTask;

public class SimulationUpdaterTask extends TimerTask {

	private GraphicDrawCoordinator drawCoordinator;

	public SimulationUpdaterTask(GraphicDrawCoordinator drawCoordinator) {
		this.drawCoordinator = drawCoordinator;
	}

	@Override
	public void run() {
		ArrayList<EdgeDevice> edgeDevices = ParameterStore.getInstance().getEdgeDevices();
		if (edgeDevices != null) {
			synchronized (edgeDevices) {
				for (EdgeDevice edgeDevice : edgeDevices) {
					edgeDevice.update();
				}
			}

			drawCoordinator.drawBins(null, null);
		}
	}
}
