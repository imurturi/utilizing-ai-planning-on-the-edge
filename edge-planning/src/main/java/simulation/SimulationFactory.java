package simulation;

import gui.drawers.GraphicDrawCoordinator;
import models.EdgeDevice;
import models.GeneratedCity;
import models.WasteBin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;
import util.ParameterStore;
import util.SHPtoPPDLGenerator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.LinkedList;

public class SimulationFactory {

	private static Logger LOG = LogManager.getLogger(SimulationFactory.class);

	private SimpleFeatureSource streetFeatureSource;
	private String simulationCoordinatorURL = "http://localhost:12345";
	private SimpleFeatureSource nodeFeatureSource;
	private GraphicDrawCoordinator graphicDrawCoordinator;

	public SimulationFactory(SimpleFeatureSource nodeFeatureSource, SimpleFeatureSource streetFeatureSource, GraphicDrawCoordinator graphicDrawCoordinator) {
		this.nodeFeatureSource = nodeFeatureSource;
		this.graphicDrawCoordinator = graphicDrawCoordinator;
		this.streetFeatureSource = streetFeatureSource;

		resetEdges();

	}

	public EdgeDevice generateEdge(int binCount, float simulationSpeed, Point nodePosition, Double atNodeId, double authorityRadius) {
		try {
			LOG.debug("Generating Edge Device with BinCount: {}, SimulationSpeed: {}, AuthorityRadius: {} at Node: {}", binCount,simulationSpeed,authorityRadius, atNodeId);
			Polygon authorityBoundingCircle = generateEdgeRadius(nodePosition,authorityRadius);
			SimpleFeatureCollection nodesInAuthority = getFeaturesInDistance(nodeFeatureSource, authorityBoundingCircle);
			SimpleFeatureCollection streetsInAuthority = getFeaturesInDistance(streetFeatureSource, authorityBoundingCircle);
			if (!nodesInAuthority.isEmpty()) {
				int edgeNr = ParameterStore.getInstance().getEdgeDevices() == null ? 0 : ParameterStore.getInstance().getEdgeDevices().size();
				String simulationID = "e" + edgeNr;
				String simulationURL = startSimulation(simulationID, binCount, simulationSpeed);
				LOG.debug("Simulation for Edge Device with SimulationID: {} started.",simulationID);

				if(simulationURL != null){
					EdgeDevice edgeDevice = new EdgeDevice(simulationID, atNodeId, authorityRadius, simulationURL, graphicDrawCoordinator);
					SHPtoPPDLGenerator cityGenerator = new SHPtoPPDLGenerator();
					GeneratedCity generatedCity = cityGenerator.generateCityPPDL(streetsInAuthority, nodesInAuthority);
					LOG.debug("{}: Nodes in Authority: {}, Streets in Authority: {}", simulationID, nodesInAuthority.size(), streetsInAuthority.size());

					BinFactory binFactory = new BinFactory(generatedCity, nodesInAuthority, simulationID);
					LinkedList<WasteBin> generatedBins = binFactory.randomizeBinLocation(binCount);
					if(generatedBins == null)
						return null;

					edgeDevice.setGeneratedCity(generatedCity);
					edgeDevice.setAuthorityZone(nodesInAuthority.getBounds());
					edgeDevice.setWasteBins(generatedBins);
					edgeDevice.setNodePosition(nodePosition);

					return edgeDevice;
				}
			}


		} catch (IOException e) {
			e.printStackTrace();
		}


		return null;
	}

	public String startSimulation(String simulationId, int binCount, float speed) {
		URL simulationURL = null;
		try {
			simulationURL = new URL(simulationCoordinatorURL + "/start?simulationid=" + simulationId + "&num_bins=" + binCount + "&speed=" + speed);
			HttpURLConnection httpURLConnection = (HttpURLConnection) simulationURL.openConnection();
			httpURLConnection.setRequestMethod("GET");
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			return bufferedReader.readLine();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Simulation Coordinator is not running!");
			e.printStackTrace();
		}

		return null;
	}


	public SimpleFeatureCollection getFeaturesInDistance(SimpleFeatureSource featureSource, Polygon boundingCircle) throws IOException {

		FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
		Filter filter =
				ff.intersects(ff.property("the_geom"), ff.literal(boundingCircle));

		return featureSource.getFeatures(filter);
	}

	public Polygon generateEdgeRadius(Point center, double radius){
		GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
		shapeFactory.setNumPoints(64); // adjustable
		shapeFactory.setCentre(center.getCoordinate());
		// Length in meters of 1° of latitude = always 111.32 km
		shapeFactory.setWidth(radius*2/111320d);
		// Length in meters of 1° of longitude = 40075 km * cos( latitude ) / 360
		shapeFactory.setHeight(radius*2 / (40075000 * Math.cos(Math.toRadians(center.getX())) / 360));

		return shapeFactory.createEllipse();
	}

	public void resetEdges() {
		URL simulationURL = null;
		try {
			simulationURL = new URL(simulationCoordinatorURL + "/reset");
			HttpURLConnection httpURLConnection = (HttpURLConnection) simulationURL.openConnection();
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.getInputStream();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Simulation Coordinator is not running!");
			e.printStackTrace();
		}
	}
}
