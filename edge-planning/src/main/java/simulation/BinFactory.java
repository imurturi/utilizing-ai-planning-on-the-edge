package simulation;

import models.GeneratedCity;
import models.WasteBin;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import javax.swing.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class BinFactory {

    private GeneratedCity generatedCity;
    private String binIdPrefix;
    private SimpleFeatureCollection nodeCollection;

    public BinFactory(GeneratedCity generatedCity, SimpleFeatureCollection nodeCollection, String binIdPrefix) {
        this.generatedCity = generatedCity;
        this.binIdPrefix = binIdPrefix;
        this.nodeCollection = nodeCollection;
    }

    public LinkedList<WasteBin> randomizeBinLocation(int binCount) {
        if (generatedCity != null) {
            LinkedList<WasteBin> wasteBins = new LinkedList<>();
            List<Integer> assignedNodeIndices = new ArrayList<>();
            List<Double> nodes = generatedCity.getNodeIds();
            if (binCount > nodes.size()) {
                JOptionPane.showMessageDialog(null, "Not enough nodes in radius to place all bins. Please increase authority radius!", "Error", JOptionPane.ERROR_MESSAGE);
                return null;
            }
            Random random = new Random(System.currentTimeMillis());
            final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

            for (int i = 0; i < binCount; i++) {
                int randomIndex = random.nextInt(nodes.size() - 1);
                while (assignedNodeIndices.contains(randomIndex))
                    randomIndex = random.nextInt(nodes.size() - 1);
                Double nodeId = nodes.get(randomIndex);
                if (nodeId != null) {
                    WasteBin bin = new WasteBin(nodeId, binIdPrefix + "bin" + i);
                    bin.setFullness(0);

                    Filter nodeFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(nodeId));
                    FeatureCollection<SimpleFeatureType, SimpleFeature> filteredFeatures = nodeCollection.subCollection(nodeFilter);

                    if (filteredFeatures.size() == 1) {
                        FeatureIterator<SimpleFeature> featureIterator = filteredFeatures.features();
                        SimpleFeature node = featureIterator.next();
                        featureIterator.close();
                        Geometry geom = (Geometry) node.getDefaultGeometry();

                        org.locationtech.jts.geom.Point nodePositionCenter = (org.locationtech.jts.geom.Point) geom.getGeometryN(0);
                        bin.setNodeWorldPositon(nodePositionCenter.getCoordinate());
                    }

                    wasteBins.add(bin);
                    assignedNodeIndices.add(randomIndex);
                }
            }

            return wasteBins;

        }
        return null;
    }
}
