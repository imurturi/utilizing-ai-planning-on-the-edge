import gui.ViennaPlannerUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class Main {

	public static void main(String[] args) throws Exception {
		System.setProperty("awt.useSystemAAFontSettings", "on");
		System.setProperty("swing.aatext", "true");

		UIManager.getLookAndFeelDefaults().put("Dracula", "com.bulenkov.darcula.DarculaLaf");
		UIManager.setLookAndFeel("com.bulenkov.darcula.DarculaLaf");

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ViennaPlannerUI plannerUI = new ViennaPlannerUI();
				plannerUI.setContentPane(plannerUI.getMainPanel());
				plannerUI.setSize(640, 480);
				plannerUI.setVisible(true);
			}
		});

	}


}

