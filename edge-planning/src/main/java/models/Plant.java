package models;

import java.util.Objects;

public class Plant {
	private String id;
	private Double atNodeId;

	public Plant(String id, Double atNodeId) {
		this.id = id;
		this.atNodeId = atNodeId;
	}

	public String getId() {
		return id;
	}

	public Double getAtNodeId() {
		return atNodeId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Plant plant = (Plant) o;
		return Objects.equals(atNodeId, plant.atNodeId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(atNodeId);
	}
}
