package models;

import gui.drawers.GraphicDrawCoordinator;
import models.actions.Action;
import models.actions.BinEmptyAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Point;
import simulation.SimulationConnector;
import simulation.SimulationStatus;
import util.ParameterStore;
import util.UseCaseObserver;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class EdgeDevice implements Comparable<EdgeDevice> {

	private static Logger LOG = LogManager.getLogger(EdgeDevice.class);

	private String simulationID;
	private Double atNodeId;

	private ReferencedEnvelope authorityZone;
	private LinkedList<WasteBin> wasteBins;
	private ArrayList<Vehicle> leasedVehicles;
	private ArrayList<Plant> plants;

	private Double authorityRadius;
	private String simulationURL;
	private GeneratedCity generatedCity;

	private SimulationConnector simulationConnector;
	private GraphicDrawCoordinator drawCoordinator;
	private Point nodePosition;

	private ArrayList<Plan> currentPlans = null;

	private Integer lastUpdateTime = null;

	public EdgeDevice(String simulationID, Double atNodeId, Double authorityRadius, String simulationURL, GraphicDrawCoordinator drawCoordinator) {
		this.simulationID = simulationID;
		this.atNodeId = atNodeId;
		this.simulationURL = simulationURL;
		this.drawCoordinator = drawCoordinator;
		this.authorityRadius = authorityRadius;
	}

	public ReferencedEnvelope getAuthorityZone() {
		return authorityZone;
	}

	public void setAuthorityZone(ReferencedEnvelope authorityZone) {
		this.authorityZone = authorityZone;
	}

	public LinkedList<WasteBin> getWasteBins() {
		return wasteBins;
	}

	public LinkedList<WasteBin> getWasteBinsNotInPlanning(int wasteBinMinimum) {
		LinkedList<WasteBin> wasteBinsNotInPlanning = new LinkedList<>(wasteBins);
		synchronized (wasteBins) {
			wasteBinsNotInPlanning.removeAll(wasteBins.stream().filter(wasteBin -> (wasteBin.getFullness() <= Double.valueOf(wasteBinMinimum))).collect(Collectors.toList()));
		}
		if(currentPlans != null && !currentPlans.isEmpty()) {
			for (Plan currentPlan : currentPlans) {
				List<Action> emptyBinActions = currentPlan.getActions().stream().filter(action -> (action.getActionType() == Action.TYPE.BINEMPTY)).collect(Collectors.toList());
				for (Action emptyBinAction : emptyBinActions) {
					BinEmptyAction binEmptyAction = (BinEmptyAction) emptyBinAction;
					wasteBinsNotInPlanning.removeAll(wasteBins.stream().filter(wasteBin -> (wasteBin.getId().equals(binEmptyAction.getBin()))).collect(Collectors.toList()));
				}
			}
		}
		return wasteBinsNotInPlanning;
	}

	public void setWasteBins(LinkedList<WasteBin> wasteBins) {
		this.wasteBins = wasteBins;
	}

	public Double getAtNodeId() {
		return atNodeId;
	}

	public Double getAuthorityRadius() {
		return authorityRadius;
	}

	public GeneratedCity getGeneratedCity() {
		return generatedCity;
	}

	public void setGeneratedCity(GeneratedCity generatedCity) {
		this.generatedCity = generatedCity;
	}

	public void addVehicleLease(Vehicle vehicle) {
		if (leasedVehicles == null)
			leasedVehicles = new ArrayList<>();
		leasedVehicles.add(vehicle);
	}

	public void addPlant(Plant plant) {
		if (plants == null)
			plants = new ArrayList<>();
		plants.add(plant);
	}

	public ArrayList<Vehicle> getLeasedVehicles() {
		return leasedVehicles;
	}

	public ArrayList<Plant> getPlants() {
		return plants;
	}

	public String getSimulationID() {
		return simulationID;
	}

	public boolean isWorkToBeDone(int wasteBinMinimum) {
		return !(wasteBins.parallelStream().filter(wasteBin -> (wasteBin.getFullness() >= wasteBinMinimum)).count() == 0);
	}

	public int numBinsFull(int wasteBinMinimum) {
		return (int) wasteBins.parallelStream().filter(wasteBin -> (wasteBin.getFullness() >= wasteBinMinimum)).count();
	}

	public Coordinate getNodeCoordinate() {
		if (nodePosition == null)
			return null;
		return nodePosition.getCoordinate();
	}

	public void setNodePosition(Point nodePosition) {
		this.nodePosition = nodePosition;
	}

	public void setCurrentPlans(ArrayList<Plan> currentPlans) {
		this.currentPlans = currentPlans;
	}

	public void update() {
		if (simulationConnector == null)
			simulationConnector = new SimulationConnector(simulationURL);

		SimulationStatus simulationStatus = simulationConnector.getSimulationStatus();
		LOG.trace("Updating Edge Device {}", simulationID);

		if (simulationStatus != null) {

			if (lastUpdateTime == null)
				lastUpdateTime = simulationStatus.getCurrent_day() * 24 + simulationStatus.getCurrent_hour();

			Integer timeElapsedSinceLastUpdate = simulationStatus.getCurrent_day() * 24 + simulationStatus.getCurrent_hour() - lastUpdateTime;

			lastUpdateTime = simulationStatus.getCurrent_day() * 24 + simulationStatus.getCurrent_hour();

			synchronized (wasteBins) {
				for (int i = 0; i < simulationStatus.getNum_bins(); i++) {
					WasteBin bin = wasteBins.get(i);
					if (bin != null) {
						int indexStartsAt = bin.getId().indexOf("bin");
						int binIndex = Integer.valueOf(bin.getId().substring(indexStartsAt + 3));
						float newFullness = simulationStatus.getBin_fullness()[binIndex];
						if (bin.getFullness() == 100 && newFullness == 100)
							UseCaseObserver.getInstance().increaseTimeWasteBinFull(this, bin, timeElapsedSinceLastUpdate);
						bin.setFullness(newFullness);

					}
				}
			}

			if (drawCoordinator != null)
				drawCoordinator.drawBins(wasteBins, null);
		}

	}

	public void emptyBin(WasteBin wasteBin) {
		if (wasteBins.contains(wasteBin)) {
			if (simulationConnector == null)
				simulationConnector = new SimulationConnector(simulationURL);
			int indexStartsAt = wasteBin.getId().indexOf("bin");
			int binIndex = Integer.valueOf(wasteBin.getId().substring(indexStartsAt + 3));
			UseCaseObserver.getInstance().addBinEmptied(this);
			synchronized (wasteBins) {
				wasteBins.parallelStream().filter(wasteBin1 -> wasteBin1.equals(wasteBin)).forEach(wasteBin1 -> wasteBin1.setFullness(0));
			}
			simulationConnector.emptyBin(binIndex);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EdgeDevice that = (EdgeDevice) o;
		return Objects.equals(simulationID, that.simulationID);
	}

	@Override
	public int hashCode() {
		return Objects.hash(simulationID);
	}


	@Override
	public int compareTo(EdgeDevice o) {
		int minFullness = ParameterStore.getInstance().getMinFullness();
		boolean thisHasWork = this.isWorkToBeDone(minFullness);
		boolean otherHasWork = o.isWorkToBeDone(minFullness);
		boolean thisHasVehicles = this.leasedVehicles != null && !this.leasedVehicles.isEmpty();
		boolean otherHasVehicles = o.leasedVehicles != null && !o.leasedVehicles.isEmpty();
		if (thisHasWork && !otherHasWork)
			return -1;
		if (otherHasWork && !thisHasWork)
			return 1;
		if (thisHasVehicles && !otherHasVehicles)
			return -1;
		if (otherHasVehicles && !thisHasVehicles)
			return 1;

		return o.numBinsFull(minFullness) - this.numBinsFull(minFullness);
	}

	@Override
	public String toString() {
		return "EdgeDevice{" +
				"simulationID='" + simulationID + '\'' +
				", atNodeId=" + atNodeId +
				", wasteBins=" + wasteBins.size() +
				", authorityRadius=" + authorityRadius +
				", generatedCity=" + generatedCity +
				", leasedVehicleCount=" + leasedVehicles.size() +
				'}';
	}
}
