package models;

import org.opengis.filter.identity.FeatureId;

import java.util.Objects;

public class StreetFeature {
	private FeatureId id;
	private Double distance;
	private Double[] connectedIds = new Double[2];

	public StreetFeature(FeatureId id) {
		this.id = id;
	}

	public FeatureId getId() {
		return id;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Double[] getConnectedIds() {
		return connectedIds;
	}

	public void setConnectedIds(Double[] connectedIds) {
		this.connectedIds = connectedIds;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		StreetFeature that = (StreetFeature) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
