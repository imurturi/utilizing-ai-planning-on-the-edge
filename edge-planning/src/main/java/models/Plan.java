package models;

import models.actions.Action;

import java.util.*;

public class Plan {
	private HashMap<String, Vehicle> vehicles;
	private LinkedList<Action> actions;
	private float cost;
	private float planDuration;
	private EdgeDevice edgeDevice;


	public Plan(HashMap<String, Vehicle>  vehicles, LinkedList<Action> actions, float cost) {
		this.vehicles = vehicles;
		this.actions = actions;
		this.cost = cost;
	}

	public HashMap<String, Vehicle>  getVehicles() {
		return vehicles;
	}

	public LinkedList<Action> getActions() {
		return actions;
	}

	public float getCost() {
		return cost;
	}

	public float getPlanDuration() {
		return planDuration;
	}

	public void setPlanDuration(float planDuration) {
		this.planDuration = planDuration;
	}

	public void setEdgeDevice(EdgeDevice edgeDevice) {
		this.edgeDevice = edgeDevice;
	}

	private String getID() {
		return edgeDevice.getSimulationID()+vehicles.size()+getPlanDuration()+getCost();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Plan plan = (Plan) o;
		return Objects.equals(getID(), plan.getID());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getID());
	}
}
