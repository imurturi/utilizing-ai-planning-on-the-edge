package models;

import org.geotools.data.simple.SimpleFeatureCollection;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class GeneratedCity {
	private File generatedCityPPDL;
	private List<Double> nodeIds;
	private HashMap<String, String> streetNameMap;
	private HashMap<Double, String> nodeNameMap;
	private SimpleFeatureCollection nodesInAuthority;

	public GeneratedCity(File generatedCityPPDL, List<Double> nodeIds) {
		this.generatedCityPPDL = generatedCityPPDL;
		this.nodeIds = nodeIds;
	}

	public File getGeneratedCityPPDL() {
		return generatedCityPPDL;
	}

	public List<Double> getNodeIds() {
		return nodeIds;
	}

	public HashMap<String, String> getStreetNameMap() {
		return streetNameMap;
	}

	public void setStreetNameMap(HashMap<String, String> streetNameMap) {
		this.streetNameMap = streetNameMap;
	}

	public HashMap<Double, String> getNodeNameMap() {
		return nodeNameMap;
	}

	public void setNodeNameMap(HashMap<Double, String> nodeNameMap) {
		this.nodeNameMap = nodeNameMap;
	}

	@Override
	public String toString() {
		return "GeneratedCity{" +
				"StreetCount=" + streetNameMap.size() +
				", NodeCount=" + nodeNameMap.size() +
				'}';
	}

	public void setNodesInAuthority(SimpleFeatureCollection nodeFeatureCollection) {
		this.nodesInAuthority = nodeFeatureCollection;
	}

	public SimpleFeatureCollection getNodesInAuthority() {
		return nodesInAuthority;
	}
}
