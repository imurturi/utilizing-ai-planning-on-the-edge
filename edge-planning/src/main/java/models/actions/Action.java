package models.actions;

import models.Vehicle;

public class Action {

	public enum TYPE {
		MOVE, BINEMPTY, TRUCKEMPTY
	}

	private TYPE actionType;

	private Vehicle vehicle;


	public Action(TYPE actionType, Vehicle vehicle) {
		this.actionType = actionType;
		this.vehicle = vehicle;
	}

	public TYPE getActionType() {
		return actionType;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}
}
