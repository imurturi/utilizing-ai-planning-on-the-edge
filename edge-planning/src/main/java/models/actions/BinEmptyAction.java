package models.actions;

import models.Vehicle;

public class BinEmptyAction extends Action{
	private Double atNode;
	private String bin;

	public BinEmptyAction(Double atNode, String bin, Vehicle vehicle) {
		super(TYPE.BINEMPTY, vehicle);
		this.atNode = atNode;
		this.bin = bin;
	}

	public Double getAtNode() {
		return atNode;
	}

	public String getBin() {
		return bin;
	}
}
