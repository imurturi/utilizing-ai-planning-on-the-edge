package models.actions;

import models.Vehicle;

public class TruckEmptyAction extends Action{
	private String plant;
	private Double atNode;

	public TruckEmptyAction(String plant, Double atNode, Vehicle vehicle) {
		super(TYPE.TRUCKEMPTY, vehicle);
		this.plant = plant;
		this.atNode = atNode;
	}

	public String getPlant() {
		return plant;
	}

}
