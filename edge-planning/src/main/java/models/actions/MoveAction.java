package models.actions;

import models.Vehicle;

public class MoveAction extends Action{

	private Double fromNode;
	private Double toNode;
	private String street;

	public MoveAction(Double fromNode, Double toNode, String street, Vehicle vehicle) {
		super(TYPE.MOVE, vehicle);
		this.fromNode = fromNode;
		this.toNode = toNode;
		this.street = street;
	}

	public Double getToNode() {
		return toNode;
	}

	public String getStreet() {
		return street;
	}
}
