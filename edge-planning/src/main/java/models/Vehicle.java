package models;

import org.jdesktop.core.animation.timing.Animator;
import org.locationtech.jts.geom.Coordinate;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;

public class Vehicle {
	private String id;
	private Double atNodeId;
	private int capacity;
	private LinkedList<String> route;
	private Color color;
	private Coordinate atCoordinate;

	private Animator animator;
	private Image VEHICLE_IMAGE;
	private Point screenPosition;

	public Vehicle(String id, Double atNodeId, Coordinate atCoordinate, int capacity) {
		this.id = id;
		this.atNodeId = atNodeId;
		this.capacity = capacity;
		this.route = new LinkedList<>();
		this.color = generateRandomColor();
		this.atCoordinate = atCoordinate;
		this.VEHICLE_IMAGE = new ImageIcon(getClass().getResource("/images/vehicle.png")).getImage();
	}

	private Color generateRandomColor() {
		Random rand = new Random(System.currentTimeMillis());

		// Java 'Color' class takes 3 floats, from 0 to 1.
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();

		return new Color(r,g,b);
	}

	public String getId() {
		return id;
	}

	public Double getAtNodeId() {
		return atNodeId;
	}

	public void setAtNodeId(Double atNodeId) {
		this.atNodeId = atNodeId;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void addRouteStep(String streetID){
		route.add(streetID);
	}

	public LinkedList<String> getRoute() {
		return route;
	}

	public Color getColor() {
		return color;
	}

	public Coordinate getAtCoordinate() {
		return atCoordinate;
	}

	public void setAtCoordinate(Coordinate atCoordinate) {
		this.atCoordinate = atCoordinate;
	}

	public Animator getAnimator() {
		return animator;
	}

	public void setAnimator(Animator animator) {
		this.animator = animator;
	}

	public Point getScreenPosition() {
		return screenPosition;
	}

	public void setScreenPosition(Point screenPosition) {
		this.screenPosition = screenPosition;
	}

	public void draw(Graphics2D graphics2D){
		int width = VEHICLE_IMAGE.getWidth(null);
		int height = VEHICLE_IMAGE.getHeight(null);
		graphics2D.drawImage(VEHICLE_IMAGE, screenPosition.x - width / 2, screenPosition.y - height / 2, width, height, null);
		graphics2D.setColor(Color.BLUE);
		graphics2D.setStroke(new BasicStroke(4));
		graphics2D.setFont(new Font("Roboto", Font.BOLD, 12));
		graphics2D.drawString(id.substring(5), screenPosition.x, screenPosition.y+height/12);
	}

	public void clear(Graphics2D graphics2D) {
		int width = VEHICLE_IMAGE.getWidth(null);
		int height = VEHICLE_IMAGE.getHeight(null);
		graphics2D.setBackground(new Color(0,0,0,0));
		graphics2D.clearRect(screenPosition.x - width / 2, screenPosition.y - height / 2, width, height);
	}

	public Rectangle getRectanglePosition(){
		int width = VEHICLE_IMAGE.getWidth(null);
		int height = VEHICLE_IMAGE.getHeight(null);
		return new Rectangle(screenPosition.x - width / 2, screenPosition.y - height / 2, width, height);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Vehicle vehicle = (Vehicle) o;
		return Objects.equals(id, vehicle.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "Vehicle{" +
				"id='" + id + '\'' +
				", atNodeId=" + atNodeId +
				'}';
	}
}
