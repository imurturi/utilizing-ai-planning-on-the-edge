package models;

import org.locationtech.jts.geom.Coordinate;

import java.util.Objects;

public class WasteBin {
	private Double atNode;
	private float fullness;
	private String id;

	private Coordinate nodeWorldPositon;

	public WasteBin(Double atNode, String id) {
		this.atNode = atNode;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public Double getAtNode() {
		return atNode;
	}

	public float getFullness() {
		return fullness;
	}

	public void setFullness(float fullness) {
		this.fullness = fullness;
	}

	public Coordinate getNodeWorldPositon() {
		return nodeWorldPositon;
	}

	public void setNodeWorldPositon(Coordinate nodeWorldPositon) {
		this.nodeWorldPositon = nodeWorldPositon;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		WasteBin wasteBin = (WasteBin) o;
		return id.equals(wasteBin.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "WasteBin{" +
				"fullness=" + fullness +
				", id='" + id + '\'' +
				'}';
	}
}
