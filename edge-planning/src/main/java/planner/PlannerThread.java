package planner;

import models.Vehicle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.ParameterStore;

import java.util.ArrayList;
import java.util.List;

public class PlannerThread implements Runnable {
	private static Logger LOG = LogManager.getLogger(PlannerThread.class);

	private List<PlannerTask> plannerTasks;

	public PlannerThread(List<PlannerTask> plannerTasks) {
		this.plannerTasks = plannerTasks;
	}

	@Override
	public void run() {
		if (plannerTasks != null && !plannerTasks.isEmpty()) {
			LOG.debug("Running Planning for {} Plans...", plannerTasks.size());
			plannerTasks.forEach(plannerTask -> {
				try {
					plannerTask.run();
				} catch (Exception e) {
					List<Vehicle> vehiclesInPlanner = plannerTask.getVehiclesInPlanner();
					ArrayList<Vehicle> unleasedVehicles = ParameterStore.getInstance().getUnassignedVehicles();
					ArrayList<Vehicle> vehiclesInPlanning = ParameterStore.getInstance().getLockedVehicles();

					synchronized (unleasedVehicles) {
						plannerTask.getObserver().getEdgeDevice().getLeasedVehicles().removeAll(vehiclesInPlanner);
						unleasedVehicles.addAll(plannerTask.getVehiclesInPlanner());
					}
					synchronized (vehiclesInPlanning){
						vehiclesInPlanning.removeAll(plannerTask.getVehiclesInPlanner());
					}
				}
			});
		}
	}
}
