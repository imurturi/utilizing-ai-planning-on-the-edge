package planner;

import models.Vehicle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Objects;

public class PlannerTask {

    private static Logger LOG = LogManager.getLogger(PlannerTask.class);
    private final PlanningObserver observer;

    private String planID;

    private ENHSP planner;
    private String[] plannerArgs;
    private List<Vehicle> vehiclesInPlanner;

    public PlannerTask(String domainPath, String problemPath, String plannerType, PlanningObserver observer, List<Vehicle> vehiclesInPlanner) {
        this.planID = "PLAN-" + System.currentTimeMillis() + observer.getEdgeDevice().getSimulationID() + problemPath;
        this.observer = observer;
        this.planner = setupPlanner(domainPath, problemPath, plannerType, observer);
        this.vehiclesInPlanner = vehiclesInPlanner;
    }

    private ENHSP setupPlanner(String domainPath, String problemPath, String plannerType, PlanningObserver observer) {
        LOG.debug("Planner intialization started.");
        plannerArgs = new String[]{"-o", domainPath, "-f", problemPath, "-planner", plannerType, "-dap"};
        ENHSP enhsp = new ENHSP(false, observer);
        enhsp.parseInput(plannerArgs);
        enhsp.configurePlanner();

        return enhsp;
    }

    public PlanningObserver getObserver() {
        return observer;
    }

    public List<Vehicle> getVehiclesInPlanner() {
        return vehiclesInPlanner;
    }

    public void run() throws Exception {
        if (planner != null) {
            LOG.info("Planning...");
            planner.parsingDomainAndProblem(plannerArgs);
            planner.planning();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlannerTask that = (PlannerTask) o;
        return planID.equals(that.planID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(planID);
    }
}
