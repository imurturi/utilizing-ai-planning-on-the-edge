package planner;

import gui.drawers.GraphicDrawCoordinator;
import models.EdgeDevice;
import models.Plan;
import models.Vehicle;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.ParameterStore;
import util.UseCaseObserver;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

public class PlanningObserver {
	private static Logger LOG = LogManager.getLogger(PlanningObserver.class);

	private final ActionListener planFoundListener;
	private final EdgeDevice edgeDevice;
	private PlanTranslator planTranslator;
	private GraphicDrawCoordinator drawCoordinator;

	public PlanningObserver(Collection<Vehicle> vehicles, GraphicDrawCoordinator drawCoordinator, ActionListener planFoundListener, EdgeDevice edgeDevice) {
		this.drawCoordinator = drawCoordinator;
		this.planTranslator = new PlanTranslator(convertVehicleListToHashMap(vehicles));
		this.planFoundListener = planFoundListener;
		this.edgeDevice = edgeDevice;
	}

	private HashMap<String, Vehicle> convertVehicleListToHashMap(Collection<Vehicle> vehicles) {
		HashMap<String, Vehicle> vehicleHashMap = new HashMap<>();
		vehicles.forEach(vehicle -> vehicleHashMap.put(vehicle.getId(), vehicle));

		return vehicleHashMap;
	}

	public void update(LinkedList<Pair<Float, Object>> plan, float cost, float planningTime) {
		LOG.info("Plan for Edge Device {} with cost {} found in {}s!", edgeDevice.getSimulationID(), cost, planningTime / 1000);
		Plan parsedPlan = planTranslator.parsePlan(plan, cost);
		parsedPlan.setPlanDuration(planningTime);
		parsedPlan.setEdgeDevice(edgeDevice);
		planFoundListener.actionPerformed(new ActionEvent(parsedPlan, 1, "planFound"));
		drawCoordinator.drawPlan(edgeDevice, parsedPlan);

		UseCaseObserver.getInstance().increasePlansGenerated();

		UseCaseObserver.getInstance().increaseTotalCost(cost);
		UseCaseObserver.getInstance().increasePlanningTime(planningTime);

		if (parsedPlan.getActions().isEmpty() && !edgeDevice.getLeasedVehicles().isEmpty()) {
			ArrayList<Vehicle> unassignedVehicles = ParameterStore.getInstance().getUnassignedVehicles();
			LOG.debug("{}: Removing {} vehicle leases", edgeDevice.getSimulationID(), planTranslator.getTrucks().size());
			//Ugly Workaround for unpathable Vehicles
			edgeDevice.getLeasedVehicles().forEach(vehicle -> {
				if(vehicle.getAtNodeId() != edgeDevice.getAtNodeId())
					vehicle.setAtNodeId(edgeDevice.getAtNodeId());
				else
					LOG.error("Vehicle {} already at {} still no plan found.",vehicle.getId(),edgeDevice.getSimulationID());
				UseCaseObserver.getInstance().increaseVehicleErrorCount(vehicle);
			});
			synchronized (unassignedVehicles) {
				unassignedVehicles.addAll(planTranslator.getTrucks().values());
			}
			synchronized (edgeDevice) {
				edgeDevice.getLeasedVehicles().removeAll(planTranslator.getTrucks().values());
			}
		}

	}

	public EdgeDevice getEdgeDevice() {
		return edgeDevice;
	}
}
