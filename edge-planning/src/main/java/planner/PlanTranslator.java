package planner;

import com.hstairs.ppmajal.expressions.NumEffect;
import com.hstairs.ppmajal.expressions.NumFluent;
import com.hstairs.ppmajal.transition.TransitionGround;
import models.Plan;
import models.Vehicle;
import models.actions.Action;
import models.actions.BinEmptyAction;
import models.actions.MoveAction;
import models.actions.TruckEmptyAction;
import org.apache.commons.lang3.tuple.Pair;
import util.NamingUtils;

import java.util.*;

public class PlanTranslator {
	private HashMap<String, Vehicle> trucks;

	public PlanTranslator(HashMap<String, Vehicle> trucks) {
		this.trucks = trucks;
	}

	public Plan parsePlan(LinkedList<Pair<Float, Object>> plan, float cost) {
		LinkedList<Action> actions = new LinkedList<>();
//		trucks.forEach((s, vehicle) -> vehicle.getRoute().retainAll(Collections.EMPTY_LIST));
		if(plan != null){
			for (Pair<Float, Object> floatObjectPair : plan) {
				TransitionGround step = (TransitionGround) floatObjectPair.getRight();
				if (step != null) {
					String truckName = step.getParameters().get(0).getName();
					Vehicle vehicle = trucks.get(truckName);
					if (vehicle != null) {


						switch (step.getName()) {
							case "move":
								String fromNode = step.getParameters().get(1).getName();
								String toNode = step.getParameters().get(2).getName();
								String streetName = step.getParameters().get(3).getName();

								MoveAction moveAction = new MoveAction(NamingUtils.fromNodeIDString(fromNode), NamingUtils.fromNodeIDString(toNode), NamingUtils.fromStreedIDString(streetName), vehicle);

								vehicle.addRouteStep(NamingUtils.fromStreedIDString(streetName));
								actions.add(moveAction);

								break;
							case "empty-bin":
								String binName = step.getParameters().get(1).getName();
								String atNode = step.getParameters().get(2).getName();

								BinEmptyAction binEmptyAction = new BinEmptyAction(NamingUtils.fromNodeIDString(atNode), binName, vehicle);
								actions.add(binEmptyAction);

								break;

							case "empty-truck":
								String plantName = step.getParameters().get(1).getName();
								String plantAtNode = step.getParameters().get(2).getName();

								TruckEmptyAction truckEmptyAction = new TruckEmptyAction(plantName, NamingUtils.fromNodeIDString(plantAtNode), vehicle);
								actions.add(truckEmptyAction);
								break;
						}
					}

				}
			}
		}


		return new Plan(trucks, actions, cost);
	}

	public HashMap<String, Vehicle> getTrucks() {
		return trucks;
	}
}
