package gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import gui.components.CustomScrollWheelTool;
import gui.components.JViennaMapPane;
import gui.drawers.GraphicDrawCoordinator;
import models.*;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.data.*;
import org.geotools.data.collection.SpatialIndexFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.visitor.NearestVisitor;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.*;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.styling.*;
import org.geotools.styling.Stroke;
import org.geotools.swing.DefaultRenderingExecutor;
import org.geotools.swing.action.*;
import org.geotools.swing.event.MapMouseAdapter;
import org.geotools.swing.event.MapMouseEvent;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import planner.PlannerTask;
import planner.PlannerThread;
import planner.PlanningObserver;
import simulation.*;
import util.ParameterStore;
import util.SHPtoPPDLGenerator;
import util.UseCaseObserver;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ViennaPlannerUI extends JFrame {

    private static Logger LOG = LogManager.getLogger(ViennaPlannerUI.class);

    private MapContent routeContent;
    private JPanel mainPanel;
    private JButton addVehicleBtn;
    private JToolBar toolBar;
    private JViennaMapPane mapPane;
    private JSpinner binCountSpinner;
    private JSpinner binCapacitySpinner;
    private JSpinner vehicleCapacitySpinner;
    private JSlider animationSlider;
    private JButton playButton;
    private JLabel statusBarLabel;
    private JLabel speedLabel;
    private JPanel statusBarPanel;
    private JPanel parameterPanel;
    private JButton resetButton;
    private JLabel planCostLabel;
    private JButton addEdgeBtn;
    private JSpinner speedSpinner;
    private JSpinner authorityRadiusSpinner;
    private JSlider planningTimeSlider;
    private JLabel planTimerLabel;
    private JSpinner stopSpinner;
    private JComboBox useCaseComboBox;
    private JSpinner wasteBinMinSpinner;
    private JCheckBox simulationModeCheckbox;
    private JCheckBox continuePlanningCheckBox;
    private JLabel daysElapsedLabel;
    private JMenuBar menuBar;

    private Timer simulationUpdateTimer;
    private javax.swing.Timer simulationTimer;
    private Timer planningTimer;

    private ExecutorService executor = Executors.newSingleThreadExecutor();


    private GraphicDrawCoordinator drawCoordinator;
    private SimulationFactory simulationFactory;

    private SimpleFeatureSource nodeFeatureSource;
    private SimpleFeatureSource streetFeatureSource;

    private Image vehicleImageIcon = Toolkit.getDefaultToolkit().createImage(getClass().getResource("/images/vehicle_cursor.png"));
    private Image edgeImageIcon = Toolkit.getDefaultToolkit().createImage(getClass().getResource("/images/edge_mist.png"));

    private Cursor vehicleCursor;
    private Cursor edgeCursor;

    private Integer edgesWithWork = 0;

    private int animationSpeed = 200;
    private File domainFile;


    private MapContent baseContent;
    private double maxVehicleDistance = 0.030;

    private float daysElapsed = 0;

    public ViennaPlannerUI() {
        $$$setupUI$$$();
        initializeUI();
    }

    private void initializeUI() {
        binCountSpinner.setModel(new SpinnerNumberModel(ParameterStore.getInstance().getBinCount(), 1, 512, 1));
        binCapacitySpinner.setModel(new SpinnerNumberModel(ParameterStore.getInstance().getBinCapacity(), 1, 50, 1));
        vehicleCapacitySpinner.setModel(new SpinnerNumberModel(ParameterStore.getInstance().getVehicleCapacity(), 1, 100, 10));
        speedSpinner.setModel(new SpinnerNumberModel(ParameterStore.getInstance().getSimulationSpeed(), 100, 10000, 1000));
        authorityRadiusSpinner.setModel(new SpinnerNumberModel(ParameterStore.getInstance().getAuthorityRadius(), 100, 5000, 100));
        stopSpinner.setModel(new SpinnerNumberModel(14, 1, 31, 1));
        wasteBinMinSpinner.setModel(new SpinnerNumberModel(ParameterStore.getInstance().getMinFullness(), 0, 100, 10));
        wasteBinMinSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                ParameterStore.getInstance().setMinWasteBinFullness((int) wasteBinMinSpinner.getValue());
            }
        });

        vehicleCursor = Toolkit.getDefaultToolkit().createCustomCursor(vehicleImageIcon, new Point(0, 0), "Add Vehicle");
        edgeCursor = Toolkit.getDefaultToolkit().createCustomCursor(edgeImageIcon, new Point(0, 0), "Add Edge Device");

        addVehicleBtn.addActionListener(addVehicleActionListener);
        addEdgeBtn.addActionListener(addEdgeActionListener);
        playButton.addActionListener(playButtonActionListener);
        resetButton.addActionListener(resetButtonActionListener);

        simulationModeCheckbox.setSelected(ParameterStore.getInstance().isSimulationMode());

        simulationModeCheckbox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                ParameterStore.getInstance().setSimulationMode(simulationModeCheckbox.isSelected());
                if (simulationModeCheckbox.isSelected())
                    mapPane.getVehicleImageGraphics().dispose();
            }
        });

        useCaseComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (useCaseComboBox.getSelectedIndex() == 0)
                    return;
                FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
                if (useCaseComboBox.getSelectedIndex() == 1) {
                    UseCaseObserver.getInstance().initUsecase1();
                } else if (useCaseComboBox.getSelectedIndex() == 2) {
                    UseCaseObserver.getInstance().initUsecase2();
                } else if (useCaseComboBox.getSelectedIndex() == 3) {
                    UseCaseObserver.getInstance().initUsecase3();
                }

                List<Double> edgeNodeIds = UseCaseObserver.getInstance().getUseCaseEdgeNodeIds();
                List<Double> vehicleNodeIds = UseCaseObserver.getInstance().getUseCaseVehicleNodeIds();
                edgeNodeIds.forEach(edgeNodeID -> {
                    Filter nodeFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(edgeNodeID));
                    try {
                        SimpleFeatureCollection foundFeatures = nodeFeatureSource.getFeatures(nodeFilter);
                        addEdgeAtFeature(foundFeatures.features());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                vehicleNodeIds.forEach(vehicleNodeID -> {
                    Filter nodeFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(vehicleNodeID));
                    try {
                        SimpleFeatureCollection foundFeatures = nodeFeatureSource.getFeatures(nodeFilter);
                        addVehicleAtFeature(foundFeatures.features());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });


            }
        });

        animationSlider.addChangeListener(animationSpeedChangeListener);
        planningTimeSlider.addChangeListener(plannerSpeedChangeListener);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                if (simulationFactory != null)
                    simulationFactory.resetEdges();
            }
        });

        mapPane.addMouseListener(mapMouseAdapter);

        ParameterStore.getInstance().setAnimationSpeed(getAnimationStepSpeedInMS());
        ParameterStore.getInstance().setSimulationStepSpeed(animationSlider.getValue());
    }


    private void createUIComponents() {
        StreamingRenderer renderer = new StreamingRenderer();
        renderer.setMapContent(routeContent);

        RenderingHints hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        hints.add(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED));
        renderer.setJava2DHints(hints);

        HashMap rendererParams = new HashMap();
        rendererParams.put("optimizedDataLoadingEnabled", Boolean.valueOf(true));

        renderer.setRendererHints(rendererParams);

        mapPane = new JViennaMapPane(new DefaultRenderingExecutor(), renderer);

        createToolbar();
        createMenuBar();
    }

    private void createMenuBar() {
        menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        JMenuItem streetFileMenuItem = new JMenuItem("Load Shapefiles...");
        JMenuItem defaultMenuItem = new JMenuItem("Load Defaults");
        JMenuItem exitMenuItem = new JMenuItem("Exit");
        streetFileMenuItem.addActionListener(e -> {

            JFileChooser fileChooser = new JFileChooser("~/");
            fileChooser.setFileFilter(new FileNameExtensionFilter("Street Shapefile", "shp"));
            fileChooser.setDialogTitle("Select Street Shapefile...");
            if (fileChooser.showOpenDialog(getMainPanel()) == JFileChooser.APPROVE_OPTION) {
                try {
                    loadStreetShapefile(fileChooser.getSelectedFile(), false);
                    fileChooser.setCurrentDirectory(fileChooser.getCurrentDirectory());
                    fileChooser.setFileFilter(new FileNameExtensionFilter("Node Shapefile", "shp"));
                    fileChooser.setDialogTitle("Select Node Shapefile...");
                    if (fileChooser.showOpenDialog(getMainPanel()) == JFileChooser.APPROVE_OPTION) {
                        loadNodeShapefile(fileChooser.getSelectedFile());
                    }

                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }

        });

        defaultMenuItem.addActionListener(e -> {
            try {
                File streetFile = new File("shapefile" + File.separator + "STRASSENGRAPHOGD.shp");
                File nodeFile = new File("shapefile" + File.separator + "STRASSENKNOTENOGD.shp");

                loadStreetShapefile(streetFile, true);
                loadNodeShapefile(nodeFile);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        exitMenuItem.addActionListener(e -> System.exit(0));


        menu.add(streetFileMenuItem);
        menu.add(defaultMenuItem);
        menu.add(exitMenuItem);
        menuBar.add(menu);
        setJMenuBar(menuBar);
    }


    public void loadStreetShapefile(File streetShapeFile, boolean loadVisualLayers) throws IOException {
        if (streetShapeFile != null && streetShapeFile.canRead()) {
            streetFeatureSource = getFeatureSourceFromFile(streetShapeFile);
            final SpatialIndexFeatureCollection cachedStreetCollection =
                    new SpatialIndexFeatureCollection(streetFeatureSource.getFeatures());
            final SimpleFeatureSource cachedStreetSource = DataUtilities.source(cachedStreetCollection);

            if (routeContent == null) {
                routeContent = new MapContent();
            }

            routeContent.setTitle("Vienna WasteMap");
            Style streetStyle = SLD.createSimpleStyle(streetFeatureSource.getSchema());

            FeatureLayer baseStreetLayer = new FeatureLayer(cachedStreetSource, streetStyle);
            FeatureLayer routeLayer = new FeatureLayer(cachedStreetSource, streetStyle);
            if (baseContent == null)
                baseContent = new MapContent();
            baseContent.addLayer(baseStreetLayer);

            if (loadVisualLayers) {
                File genflowFile = new File("shapefile" + File.separator + "GENFLWOGDPolygon.shp");
                File gruengewogFile = new File("shapefile" + File.separator + "GRUENGEWOGDPolygon.shp");
                SimpleFeatureSource blockShapeFeatureSource = getFeatureSourceFromFile(genflowFile);
                SimpleFeatureSource waterShapeFeatureSource = getFeatureSourceFromFile(gruengewogFile);

                final SpatialIndexFeatureCollection cachedBlockCollection =
                        new SpatialIndexFeatureCollection(blockShapeFeatureSource.getFeatures());
                final SimpleFeatureSource cachedBlockSource = DataUtilities.source(cachedBlockCollection);

                final SpatialIndexFeatureCollection cachedWaterCollection =
                        new SpatialIndexFeatureCollection(waterShapeFeatureSource.getFeatures());
                final SimpleFeatureSource cachedwaterSource = DataUtilities.source(cachedWaterCollection);


                Style blockStyle = createPolygonStyle("#a47158");
                Style waterStyle = createPolygonStyle("#4c7bb7");

                FeatureLayer blockShapeLayer = new FeatureLayer(cachedBlockSource, blockStyle);
                FeatureLayer waterShapeLayer = new FeatureLayer(cachedwaterSource, waterStyle);

                baseContent.addLayer(blockShapeLayer);
                baseContent.addLayer(waterShapeLayer);
            }

            routeContent.addLayer(routeLayer);
            mapPane.setMapContent(routeContent);
            mapPane.setBaseContent(baseContent);


            LOG.debug("Street Shapefile {} loaded.", streetShapeFile.getPath());
        }
    }


    public void loadNodeShapefile(File nodeShapeFile) throws IOException {
        if (nodeShapeFile != null && nodeShapeFile.canRead() && streetFeatureSource != null) {
            nodeFeatureSource = getFeatureSourceFromFile(nodeShapeFile);
            final SpatialIndexFeatureCollection cachedNodeCollection =
                    new SpatialIndexFeatureCollection(nodeFeatureSource.getFeatures());
            final SimpleFeatureSource cachedNodeSource = DataUtilities.source(cachedNodeCollection);

            if (routeContent == null) {
                JOptionPane.showMessageDialog(this, "Please load a street shapefile first!", "Error", JOptionPane.ERROR_MESSAGE);
            }

            Style style = SLD.createSimpleStyle(nodeFeatureSource.getSchema());
            FeatureLayer layer = new FeatureLayer(cachedNodeSource, style);
            baseContent.addLayer(layer);

            drawCoordinator = new GraphicDrawCoordinator(mapPane, nodeFeatureSource, statusBarLabel);
            mapPane.setDrawCoordinator(drawCoordinator);

            simulationFactory = new SimulationFactory(nodeFeatureSource, streetFeatureSource, drawCoordinator);

            LOG.debug("Node Shapefile {} loaded.", nodeShapeFile.getPath());

            statusBarLabel.setText("Please set parameters...");

        } else {
            JOptionPane.showMessageDialog(this, "Please load a street shapefile first!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public SimpleFeatureSource getFeatureSourceFromFile(File shapeFile) throws IOException {
        FileDataStore store = FileDataStoreFinder.getDataStore(shapeFile);
        return store.getFeatureSource();
    }

    private static Style createPolygonStyle(String colorHex) {
        StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();
        FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2();
        PolygonSymbolizer symbolizer = styleFactory.createPolygonSymbolizer();
        Fill fill = styleFactory.createFill(
                filterFactory.literal(colorHex),
                filterFactory.literal(0.5)
        );
        final Stroke stroke = styleFactory.createStroke(filterFactory.literal(Color.black),
                filterFactory.literal(1));
        symbolizer.setFill(fill);
        symbolizer.setStroke(stroke);
        Rule rule = styleFactory.createRule();
        rule.symbolizers().add(symbolizer);
        FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle();
        fts.rules().add(rule);

        Style style = styleFactory.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }


    private SimpleFeatureSource getFeaturesFromEnvelope(SimpleFeatureSource featureSource, ReferencedEnvelope envelope) {

        try {
            FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

            Filter filter = ff.intersects(ff.property("the_geom"), ff.literal(envelope));

            return DataUtilities.source(featureSource.getFeatures(filter));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    private void createToolbar() {
        this.toolBar = new JToolBar();
        this.toolBar.setOrientation(0);
        this.toolBar.setFloatable(false);
        ButtonGroup cursorToolGrp = new ButtonGroup();
        this.mapPane.addMouseListener(new CustomScrollWheelTool(this.mapPane));

        JButton btn;
        btn = new JButton(new NoToolAction(this.mapPane));
        btn.setName("ToolbarPointerButton");
        this.toolBar.add(btn);
        cursorToolGrp.add(btn);

        btn = new JButton(new ZoomInAction(this.mapPane));
        btn.setName("ToolbarZoomInButton");
        this.toolBar.add(btn);
        cursorToolGrp.add(btn);
        btn = new JButton(new ZoomOutAction(this.mapPane));
        btn.setName("ToolbarZoomOutButton");
        this.toolBar.add(btn);
        cursorToolGrp.add(btn);
        this.toolBar.addSeparator();

        btn = new JButton(new PanAction(this.mapPane));
        btn.setName("ToolbarPanButton");
        this.toolBar.add(btn);
        cursorToolGrp.add(btn);
        this.toolBar.addSeparator();


        btn = new JButton(new ResetAction(this.mapPane));
        btn.setName("ToolbarResetButton");
        this.toolBar.add(btn);
    }

    private ActionListener addVehicleActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            mapPane.setCursorTool(null);
            mapPane.setCursor(vehicleCursor);
        }
    };

    private ActionListener addEdgeActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            mapPane.setCursorTool(null);
            mapPane.setCursor(edgeCursor);
        }
    };

    private final ActionListener planActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            plan();
        }
    };

    private void plan() {
        ArrayList<EdgeDevice> edgeDevices = ParameterStore.getInstance().getEdgeDevices();
        ArrayList<Vehicle> unassignedVehicles = ParameterStore.getInstance().getUnassignedVehicles();
        ArrayList<Vehicle> vehiclesInPlanning = ParameterStore.getInstance().getLockedVehicles();

        synchronized (edgeDevices) {
            Collections.sort(edgeDevices, (o1, o2) -> {
                double minDistanceToEdge1 = Double.MAX_VALUE;
                double minDistanceToEdge2 = Double.MAX_VALUE;
                for (Vehicle unassignedVehicle : unassignedVehicles) {
                    double currDistanceToE1 = o1.getNodeCoordinate().distance(unassignedVehicle.getAtCoordinate());
                    double currDistanceToE2 = o2.getNodeCoordinate().distance(unassignedVehicle.getAtCoordinate());
                    if (currDistanceToE1 < minDistanceToEdge1)
                        minDistanceToEdge1 = currDistanceToE1;
                    if (currDistanceToE2 < minDistanceToEdge2)
                        minDistanceToEdge2 = currDistanceToE2;
                }
                return Double.compare(minDistanceToEdge1, minDistanceToEdge2);
            });
            LOG.debug("Started planning for {} Edge Devices.", edgeDevices.size());
            LOG.debug("Unassigned Vehicles: {}", unassignedVehicles.size());
            LOG.debug("Vehicles in Planning: {}", vehiclesInPlanning.size());
            int binCapacity = ParameterStore.getInstance().getBinCapacity();
            int vehicleCapacity = ParameterStore.getInstance().getVehicleCapacity();
            SHPtoPPDLGenerator generator = new SHPtoPPDLGenerator();
            List<PlannerTask> plannerTasks = new ArrayList<>();
            edgesWithWork = edgeDevices.size();
            for (EdgeDevice edgeDevice : edgeDevices) {
                boolean valid = true;
                boolean requiresMovement = false;
                boolean workToBeDone = edgeDevice.isWorkToBeDone((Integer) wasteBinMinSpinner.getValue());

                if (!workToBeDone) {
                    LOG.debug("Edge Device {} - nothing to do.", edgeDevice.getSimulationID());
                    edgesWithWork--;
                    continue;
                }

                if (unassignedVehicles != null && !unassignedVehicles.isEmpty()) {
                    for (Vehicle unassignedVehicle : unassignedVehicles) {
                        if (edgeDevice.getAuthorityZone().contains(unassignedVehicle.getAtCoordinate())) {
                            /* Vehicle Already in Authority Zone, no need to move */
                            LOG.info("Vehicle {} already in Edge Device {} Authority Zone. Leasing.", unassignedVehicle.getId(), edgeDevice.getSimulationID());
                            edgeDevice.addVehicleLease(unassignedVehicle);
                        }
                    }
                    if (edgeDevice.getLeasedVehicles() == null || edgeDevice.getLeasedVehicles().isEmpty()) {
                        /* No Vehicles in Authority Zone, requires movement */
                        LOG.debug("No Vehicle in Authority Zone of Edge Device {}. Movement Plan Required.", edgeDevice.getSimulationID());
                        requiresMovement = true;
                    } else {
                        unassignedVehicles.removeAll(edgeDevice.getLeasedVehicles());
                    }
                }


                List<Vehicle> vehicles = edgeDevice.getLeasedVehicles();
                List<Plant> plants = edgeDevice.getPlants();

                if (edgeDevices == null || edgeDevices.isEmpty()) {
                    valid = false;
                    JOptionPane.showMessageDialog(getMainPanel(), "Please add at least one Edge Device!", "Validation Error", JOptionPane.ERROR_MESSAGE);
                }

                if (plants == null || plants.isEmpty()) {
                    valid = false;
                    JOptionPane.showMessageDialog(getMainPanel(), "Please add at least one Plant!", "Validation Error", JOptionPane.ERROR_MESSAGE);
                }

                if (vehicles != null && !vehicles.isEmpty()) {
                    /* Don't use Vehicles that have a Route */
                    vehicles = vehicles.parallelStream().filter(vehicle -> (vehicle.getRoute() == null || vehicle.getRoute().isEmpty()) && (vehiclesInPlanning == null || !vehiclesInPlanning.contains(vehicle))).collect(Collectors.toList());
                    if (vehicles.isEmpty()) {
                        valid = false;
                        LOG.debug("No Vehicle without Plan found for Edge Device {}", edgeDevice.getSimulationID());
                    }
                }

                if (valid) {
                    String problemPath = null;
                    if (requiresMovement) {
                        Map<Vehicle, ReferencedEnvelope> closestVehicleMap = findNearestVehicleToEdge(edgeDevice, unassignedVehicles);
                        if (closestVehicleMap != null && closestVehicleMap.size() == 1) {
                            vehicles = new ArrayList<>();
                            vehicles.addAll(closestVehicleMap.keySet());
                            Vehicle closestVehicle = (Vehicle) closestVehicleMap.keySet().toArray()[0];


                            if (closestVehicle.getRoute().isEmpty()) {
                                edgeDevice.addVehicleLease(closestVehicle);
                                unassignedVehicles.remove(closestVehicle);

                                LOG.info("Closest Vehicle {} to Edge Device {} found. Leasing and generating movement Plan.", closestVehicle.getId(), edgeDevice.getSimulationID());

                                ReferencedEnvelope envelope = (ReferencedEnvelope) closestVehicleMap.values().toArray()[0];
                                AffineTransform transform = mapPane.getWorldToScreenTransform();
                                Point2D.Double targetMinPoint = new Point2D.Double();
                                Point2D.Double targetMaxPoint = new Point2D.Double();
                                transform.transform(new Point2D.Double(envelope.getMinX(), envelope.getMinY()), targetMinPoint);
                                transform.transform(new Point2D.Double(envelope.getMaxX(), envelope.getMaxY()), targetMaxPoint);
                                GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
                                shapeFactory.setEnvelope(envelope);

                                do {
                                    LOG.debug("Generating new City Envelope");
                                    GeneratedCity extenededCity = generator.generateCityPPDL(getFeaturesFromEnvelope(streetFeatureSource, envelope), getFeaturesFromEnvelope(nodeFeatureSource, envelope));
                                    LOG.debug("Finding nearest node in Authority");
                                    Double nearestNodeInAuthority = findNearestNodeInAuthority(closestVehicle, edgeDevice.getGeneratedCity().getNodesInAuthority());
                                    LOG.debug("Closest Node {} found.", nearestNodeInAuthority);
                                    if (nearestNodeInAuthority != null)
                                        problemPath = generator.generateMovingProblem(closestVehicle, extenededCity, nearestNodeInAuthority);
                                    else
                                        problemPath = generator.generateMovingProblem(closestVehicle, extenededCity, edgeDevice.getAtNodeId());

                                    LOG.debug("Generating Movement Problem for Edge Device {}", edgeDevice.getSimulationID());
                                    if (problemPath == null) {
                                        LOG.debug("Problem could not be generated. Increasing Envelope Size.");
                                        envelope.expandBy(0.002);
                                        transform.transform(new Point2D.Double(envelope.getMinX(), envelope.getMinY()), targetMinPoint);
                                        transform.transform(new Point2D.Double(envelope.getMaxX(), envelope.getMaxY()), targetMaxPoint);
                                        shapeFactory.setEnvelope(envelope);
                                    }
                                } while (problemPath == null);
                            } else {
                                valid = false;
                            }
                        } else {
                            valid = false;
                        }

                    } else {
                        problemPath = generator.generateEmptyingProblem(edgeDevice, vehicles, binCapacity, vehicleCapacity, (int) wasteBinMinSpinner.getValue());
                    }

                    if (valid && problemPath != null) {
                        LOG.debug("Problem file generated and valid. Intializing planner for Edge Device {} with Vehicles {}", edgeDevice, vehicles);
                        PlanningObserver observer = new PlanningObserver(vehicles, drawCoordinator, planFoundListener, edgeDevice);
                        vehiclesInPlanning.addAll(vehicles);
                        domainFile = new File("pddl" + File.separator + "domain.pddl");


                        if (domainFile != null && problemPath != null) {
                            PlannerTask plannerTask;
                            if (!requiresMovement)
                                plannerTask = new PlannerTask(domainFile.getPath(), problemPath, "opt-hrmax", observer, vehicles);
                            else
                                plannerTask = new PlannerTask(domainFile.getPath(), problemPath, "sat-hmrp", observer, vehicles);

                            plannerTasks.add(plannerTask);
                        }
                    } else {
                        /* Release Invalid Leases */
                        if (vehicles != null) {
                            if (edgeDevice.getLeasedVehicles() != null)
                                edgeDevice.getLeasedVehicles().removeAll(vehicles);
                            unassignedVehicles.addAll(vehicles);
                        }
                    }
                }

            }

            if (plannerTasks != null && !plannerTasks.isEmpty()) {
                statusBarLabel.setText("Planning...");
                executor.submit(new PlannerThread(plannerTasks));
            }
        }

    }

    private Map<Vehicle, ReferencedEnvelope> findNearestVehicleToEdge(EdgeDevice edgeDevice, ArrayList<Vehicle> unassignedVehicles) {
        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
        Vehicle closestVehicle = null;
        ReferencedEnvelope envelope = null;
        Map<Vehicle, ReferencedEnvelope> result = null;
        Coordinate edgeDeviceCenterCoordinate = null;

        if (edgeDevice != null) {

            Filter edgeDeviceNodeFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(edgeDevice.getAtNodeId()));

            List<Filter> vehicleIdsFilter = new ArrayList<>();
            unassignedVehicles.forEach(vehicle -> vehicleIdsFilter.add(ff.equals(ff.property("GIP_OBJECT"), ff.literal(vehicle.getAtNodeId()))));
            Filter vehicleNodeIdsFilter = ff.or(vehicleIdsFilter);


            try {
                SimpleFeatureCollection edgeNodeCandidates = nodeFeatureSource.getFeatures(edgeDeviceNodeFilter);
                if (edgeNodeCandidates.size() == 1) {
                    SimpleFeatureIterator iterator = edgeNodeCandidates.features();
                    SimpleFeature edgeDeviceFeature = iterator.next();

                    SimpleFeatureCollection vehicleFeatures = nodeFeatureSource.getFeatures(vehicleNodeIdsFilter);

                    NearestVisitor nearestVisitor = new NearestVisitor(ff.property("the_geom"), edgeDeviceFeature.getAttribute("the_geom"));
                    vehicleFeatures.accepts(nearestVisitor, null);
                    Object nearestMatch = nearestVisitor.getNearestMatch();
                    Filter geometryFilter = ff.equals(ff.property("the_geom"), ff.literal(nearestMatch));
                    SimpleFeatureCollection closestNodeCandidates = nodeFeatureSource.getFeatures(geometryFilter);
                    iterator.close();
                    if (closestNodeCandidates.size() == 1) {
                        SimpleFeatureIterator closestNodeIterator = closestNodeCandidates.features();
                        SimpleFeature closestVehicleNode = closestNodeIterator.next();

                        Geometry geom = (Geometry) edgeDeviceFeature.getDefaultGeometry();
                        org.locationtech.jts.geom.Point nodePositionCenter = (org.locationtech.jts.geom.Point) geom.getGeometryN(0);
                        edgeDeviceCenterCoordinate = nodePositionCenter.getCoordinate();
                        if (closestVehicleNode != null) {
                            closestVehicle = unassignedVehicles.stream().filter(vehicle -> (vehicle.getAtNodeId().equals(closestVehicleNode.getAttribute("GIP_OBJECT")))).findFirst().get();
                            Coordinate vehicleCoordinate = closestVehicle.getAtCoordinate();
                            if (vehicleCoordinate.distance(edgeDeviceCenterCoordinate) <= maxVehicleDistance) {
                                envelope = new ReferencedEnvelope(vehicleCoordinate.getX(), edgeDeviceCenterCoordinate.getX(), vehicleCoordinate.getY(), edgeDeviceCenterCoordinate.getY(), routeContent.getCoordinateReferenceSystem());
                                envelope.expandToInclude(edgeDevice.getAuthorityZone());
                                if (envelope.getHeight() > envelope.getWidth()) {
                                    Rectangle2D.Double rect = new Rectangle2D.Double(envelope.getMinX(), envelope.getMinY(), envelope.getHeight(), envelope.getHeight());
                                    envelope = new ReferencedEnvelope(rect, routeContent.getCoordinateReferenceSystem());
                                } else {
                                    Rectangle2D.Double rect = new Rectangle2D.Double(envelope.getMinX(), envelope.getMinY(), envelope.getWidth(), envelope.getWidth());
                                    envelope = new ReferencedEnvelope(rect, routeContent.getCoordinateReferenceSystem());
                                }
                                envelope.expandToInclude(vehicleCoordinate);
                                envelope.expandToInclude(edgeDeviceCenterCoordinate);

                                result = new HashMap<>();
                                result.put(closestVehicle, envelope);

                                LOG.debug("Found closest Vehicle {} to {}", closestVehicle.getId(), edgeDevice.getSimulationID());
                            }
                        }

                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    private Double findNearestNodeInAuthority(Vehicle vehicle, FeatureCollection nodesInAuthority) {
        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

        Filter edgeDeviceCenterNodeFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(vehicle.getAtNodeId()));

        try {
            SimpleFeatureCollection edgeNodeCandidates = nodeFeatureSource.getFeatures(edgeDeviceCenterNodeFilter);
            if (edgeNodeCandidates.size() == 1) {
                SimpleFeatureIterator iterator = edgeNodeCandidates.features();
                SimpleFeature vehicleNodeFeature = iterator.next();
                NearestVisitor nearestVisitor = new NearestVisitor(ff.property("the_geom"), vehicleNodeFeature.getAttribute("the_geom"));
                nodesInAuthority.accepts(nearestVisitor, null);
                Object nearestMatch = nearestVisitor.getNearestMatch();
                Filter geometryFilter = ff.equals(ff.property("the_geom"), ff.literal(nearestMatch));
                SimpleFeatureCollection closestNodeCandidates = nodeFeatureSource.getFeatures(geometryFilter);
                SimpleFeature closestNode = null;
                iterator.close();
                if (closestNodeCandidates.size() == 1) {
                    SimpleFeatureIterator closestNodeIterator = closestNodeCandidates.features();
                    closestNode = closestNodeIterator.next();
                    closestNodeIterator.close();
                    if (closestNode != null)
                        return (Double) closestNode.getAttribute("GIP_OBJECT");
                }

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public int getAnimationStepSpeedInMS() {
        animationSpeed = animationSlider.getValue();
        int speedInMSPerHour = (int) speedSpinner.getValue();
        int speedInMSPerMin = speedInMSPerHour / 60;

        return animationSpeed * speedInMSPerMin;
    }


    private MapMouseAdapter mapMouseAdapter = new MapMouseAdapter() {

        @Override
        public void onMouseClicked(MapMouseEvent ev) {
            try {

                ReferencedEnvelope envelope = ev.getEnvelopeByPixels(25);

                FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2();
                Filter filter = filterFactory.intersects(filterFactory.property("the_geom"), filterFactory.literal(envelope));

                SimpleFeatureCollection features = nodeFeatureSource.getFeatures(filter);
                SimpleFeatureIterator iter = features.features();

                if (mapPane.getCursor().equals(edgeCursor)) {
                    addEdgeAtFeature(iter);
                }

                if (mapPane.getCursor().equals(vehicleCursor)) {
                    addVehicleAtFeature(iter);
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private void addVehicleAtFeature(SimpleFeatureIterator featureIter) {
        ArrayList<Vehicle> allVehicles = ParameterStore.getInstance().getAllVehicleList();
        ArrayList<Vehicle> unassignedVehicles = ParameterStore.getInstance().getUnassignedVehicles();


        int vehicleCapacity = ParameterStore.getInstance().getVehicleCapacity();
        if (featureIter.hasNext()) {
            SimpleFeature feature = featureIter.next();
            Double atNodeId = (Double) feature.getAttribute("GIP_OBJECT");
            System.out.println("Vehicle Added at: " + atNodeId);
            Geometry geom = (Geometry) feature.getDefaultGeometry();
            org.locationtech.jts.geom.Point nodePositionCenter = (org.locationtech.jts.geom.Point) geom.getGeometryN(0);
            int vehicleCount = 0;
            if (allVehicles != null)
                vehicleCount += allVehicles.size();


            String vehicleID = "truck" + vehicleCount;
            Vehicle vehicle = new Vehicle(vehicleID, atNodeId, nodePositionCenter.getCoordinate(), vehicleCapacity);
            unassignedVehicles.add(vehicle);

            mapPane.setCursor(Cursor.getDefaultCursor());

        }
        featureIter.close();


        drawCoordinator.drawVehicles(null, null);
    }

    private void addEdgeAtFeature(SimpleFeatureIterator featureIter) {
        ArrayList<EdgeDevice> currentEdges = ParameterStore.getInstance().getEdgeDevices();
        if (currentEdges == null)
            currentEdges = new ArrayList<>();
        int binCount = (int) binCountSpinner.getValue();
        int binCapacity = (int) binCapacitySpinner.getValue();
        int vehicleCapacity = (int) vehicleCapacitySpinner.getValue();
        int simulationSpeed = (int) speedSpinner.getValue();
        int authorityRadius = (int) authorityRadiusSpinner.getValue();

        ParameterStore.getInstance().setBinCount(binCount);
        ParameterStore.getInstance().setBinCapacity(binCapacity);
        ParameterStore.getInstance().setVehicleCapacity(vehicleCapacity);
        ParameterStore.getInstance().setSimulationSpeed(simulationSpeed);
        ParameterStore.getInstance().setAuthorityRadius(authorityRadius);


        if (featureIter.hasNext()) {
            SimpleFeature feature = featureIter.next();
            Double atNodeId = (Double) feature.getAttribute("GIP_OBJECT");
            System.out.println("Edge Device Added at: " + atNodeId);
            Geometry geom = (Geometry) feature.getDefaultGeometry();

            org.locationtech.jts.geom.Point nodePositionCenter = (org.locationtech.jts.geom.Point) geom.getGeometryN(0);

            EdgeDevice edgeDevice = simulationFactory.generateEdge(binCount, simulationSpeed, nodePositionCenter, atNodeId, authorityRadius);
            if (edgeDevice != null) {
                Plant plant = new Plant("plant0", atNodeId);
                edgeDevice.addPlant(plant);
                synchronized (currentEdges) {
                    currentEdges.add(edgeDevice);
                    ParameterStore.getInstance().setEdgeDevices(currentEdges);
                }
            }

            mapPane.setCursor(Cursor.getDefaultCursor());

        }
        featureIter.close();


        drawCoordinator.setViewPortChanged(true);
        drawCoordinator.drawEdgeDevices(currentEdges, null);
        drawCoordinator.drawBins(null, null);
        if (simulationUpdateTimer == null) {
            SimulationUpdaterTask updaterTask = new SimulationUpdaterTask(drawCoordinator);
            simulationUpdateTimer = new Timer();
            simulationUpdateTimer.scheduleAtFixedRate(updaterTask, 0, simulationSpeed);
        }


        drawCoordinator.drawVehicles(null, null);
    }


    private ActionListener playButtonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            UseCaseObserver useCaseObserver = UseCaseObserver.getInstance();
            if (planningTimer != null) {
                playButton.setText("Start");
                planningTimer.cancel();
                planningTimer = null;
                UseCaseObserver.getInstance().printLogs();
                daysElapsed = 0;
            } else {
                playButton.setText("Stop");
                int speedMSPerHour = (int) speedSpinner.getValue();
                int planTimerInH = planningTimeSlider.getValue();
                int planningSpeed = planTimerInH * speedMSPerHour;
                int stopAfterDays = (int) stopSpinner.getValue();

                planningTimer = new Timer();
                planningTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        useCaseObserver.increaseTotalDaysElapsed((float) planTimerInH / 24);
                        daysElapsed += (float) planTimerInH / 24;
                        if (daysElapsed > stopAfterDays) {
                            UseCaseObserver.getInstance().printLogs();
                            UseCaseObserver.getInstance().resetStats();
                            daysElapsed = 0;
                        }
                        plan();
                    }
                }, planningSpeed, planningSpeed);
            }
            if (simulationTimer != null && simulationTimer.isRunning())
                simulationTimer.stop();
            else {
                animationSpeed = animationSlider.getValue();
                int speedInMSPerHour = (int) speedSpinner.getValue();
                int speedInMSPerMin = speedInMSPerHour / 60;
                int finalSpeedInMS = speedInMSPerMin;
                if (simulationModeCheckbox.isSelected())
                    finalSpeedInMS = speedInMSPerMin / 60;
                simulationTimer = new javax.swing.Timer((int) (animationSpeed * finalSpeedInMS), new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                if (drawCoordinator != null) {
                                    boolean stepsLeft = drawCoordinator.drawNextStep(simulationModeCheckbox.isSelected());
                                    daysElapsedLabel.setText("Days Elapsed: " + (int) daysElapsed + " | Average Full Time: " + (int) useCaseObserver.getAverageFullTime() + "h" + " | Total Bins Emptied: " + useCaseObserver.getBinsEmptied());
                                    if (!stepsLeft && edgesWithWork != 0 && continuePlanningCheckBox.isSelected()) {
                                        plan();
                                    }
                                }
                            }
                        });
                    }
                });
                simulationTimer.start();
            }
        }
    };

    private ActionListener resetButtonActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (simulationUpdateTimer != null) {
                simulationUpdateTimer.cancel();
                simulationUpdateTimer = null;
            }
            if (simulationFactory != null)
                simulationFactory.resetEdges();
            ParameterStore.getInstance().setEdgeDevices(new ArrayList<>());
            ParameterStore.getInstance().setUnassignedVehicles(new ArrayList<>());
            ParameterStore.getInstance().setLockedVehicles(new ArrayList<>());
            drawCoordinator.resetPlans();

            drawCoordinator.setViewPortChanged(true);
            mapPane.dispose();

        }
    };

    private ActionListener planFoundListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Plan plan = (Plan) e.getSource();
            ArrayList<Vehicle> vehiclesInPlanning = ParameterStore.getInstance().getLockedVehicles();

            if (plan != null) {
                synchronized (vehiclesInPlanning) {
                    if (vehiclesInPlanning != null)
                        vehiclesInPlanning.removeAll(plan.getVehicles().values());
                }
                planCostLabel.setText("Total km driven: " + (int) UseCaseObserver.getInstance().getTotalCost() / 1000 + "km.");
            }
        }
    };

    private ChangeListener animationSpeedChangeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            animationSpeed = animationSlider.getValue();
            if (!simulationModeCheckbox.isSelected()) {
                if (animationSpeed < 60)
                    speedLabel.setText(animationSpeed + " min / Step");
                else
                    speedLabel.setText(animationSpeed / 60 + " h / Step");
            } else {
                if (animationSpeed < 60)
                    speedLabel.setText(animationSpeed + " sec / Step");
                else
                    speedLabel.setText(animationSpeed / 60 + " min / Step");
            }

            ParameterStore.getInstance().setAnimationSpeed(getAnimationStepSpeedInMS());
            ParameterStore.getInstance().setSimulationStepSpeed(animationSlider.getValue());
        }
    };

    private ChangeListener plannerSpeedChangeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            int plannerSpeed = planningTimeSlider.getValue();
            if (plannerSpeed < 24)
                planTimerLabel.setText("Plan every " + (plannerSpeed) + " h");
            else
                planTimerLabel.setText("Plan every " + (plannerSpeed / 24) + " d");
        }
    };

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout(0, 0));
        mainPanel.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        toolBar.setFloatable(false);
        toolBar.setOrientation(1);
        panel1.add(toolBar, BorderLayout.EAST);
        final Spacer spacer1 = new Spacer();
        toolBar.add(spacer1);
        addEdgeBtn = new JButton();
        addEdgeBtn.setHorizontalAlignment(0);
        addEdgeBtn.setHorizontalTextPosition(0);
        addEdgeBtn.setIcon(new ImageIcon(getClass().getResource("/images/edge_mist.png")));
        addEdgeBtn.setText("");
        addEdgeBtn.setToolTipText("Place a facility HQ on the map");
        toolBar.add(addEdgeBtn);
        addVehicleBtn = new JButton();
        addVehicleBtn.setHorizontalTextPosition(0);
        addVehicleBtn.setIcon(new ImageIcon(getClass().getResource("/images/vehicle.png")));
        addVehicleBtn.setRolloverEnabled(true);
        addVehicleBtn.setText("");
        addVehicleBtn.setToolTipText("Place a Vehicle on the Map");
        toolBar.add(addVehicleBtn);
        statusBarPanel = new JPanel();
        statusBarPanel.setLayout(new GridLayoutManager(4, 39, new Insets(0, 10, 0, 5), -1, -1));
        panel1.add(statusBarPanel, BorderLayout.SOUTH);
        parameterPanel = new JPanel();
        parameterPanel.setLayout(new GridBagLayout());
        statusBarPanel.add(parameterPanel, new GridConstraints(0, 9, 3, 30, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        parameterPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Simulation Settings"));
        final JLabel label1 = new JLabel();
        label1.setHorizontalAlignment(0);
        label1.setHorizontalTextPosition(0);
        label1.setText("# Bins");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        parameterPanel.add(label1, gbc);
        final JLabel label2 = new JLabel();
        label2.setText("Vehicle Capacity");
        gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new Insets(0, 10, 0, 5);
        parameterPanel.add(label2, gbc);
        vehicleCapacitySpinner = new JSpinner();
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(0, 0, 0, 5);
        parameterPanel.add(vehicleCapacitySpinner, gbc);
        final JLabel label3 = new JLabel();
        label3.setText("Speed (ms/h)");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new Insets(0, 0, 0, 5);
        parameterPanel.add(label3, gbc);
        speedSpinner = new JSpinner();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        parameterPanel.add(speedSpinner, gbc);
        binCountSpinner = new JSpinner();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        parameterPanel.add(binCountSpinner, gbc);
        final JLabel label4 = new JLabel();
        label4.setText("Edge Authority Radius (m)");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 5);
        parameterPanel.add(label4, gbc);
        final JLabel label5 = new JLabel();
        label5.setBackground(new Color(-12828863));
        label5.setText("Bin Cargo Size");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new Insets(0, 10, 0, 5);
        parameterPanel.add(label5, gbc);
        binCapacitySpinner = new JSpinner();
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        parameterPanel.add(binCapacitySpinner, gbc);
        authorityRadiusSpinner = new JSpinner();
        gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(0, 0, 0, 10);
        parameterPanel.add(authorityRadiusSpinner, gbc);
        animationSlider = new JSlider();
        animationSlider.setMajorTickSpacing(5);
        animationSlider.setMaximum(60);
        animationSlider.setMinimum(1);
        animationSlider.setMinorTickSpacing(0);
        animationSlider.setPaintLabels(false);
        animationSlider.setPaintTicks(true);
        animationSlider.setPaintTrack(true);
        animationSlider.setSnapToTicks(true);
        animationSlider.setToolTipText("Sets the animation speed in ms");
        animationSlider.setValue(16);
        animationSlider.setValueIsAdjusting(false);
        statusBarPanel.add(animationSlider, new GridConstraints(0, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        playButton = new JButton();
        playButton.setEnabled(true);
        playButton.setText("Start");
        statusBarPanel.add(playButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, 1, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        speedLabel = new JLabel();
        speedLabel.setText("15 min / Step");
        statusBarPanel.add(speedLabel, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        planTimerLabel = new JLabel();
        planTimerLabel.setHorizontalAlignment(11);
        planTimerLabel.setText("Plan every 1h");
        statusBarPanel.add(planTimerLabel, new GridConstraints(1, 3, 2, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        planningTimeSlider = new JSlider();
        planningTimeSlider.setMajorTickSpacing(24);
        planningTimeSlider.setMaximum(168);
        planningTimeSlider.setMinimum(1);
        planningTimeSlider.setMinorTickSpacing(2);
        planningTimeSlider.setPaintLabels(false);
        planningTimeSlider.setPaintTicks(true);
        planningTimeSlider.setSnapToTicks(false);
        planningTimeSlider.setValue(1);
        planningTimeSlider.setValueIsAdjusting(false);
        statusBarPanel.add(planningTimeSlider, new GridConstraints(1, 0, 2, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        useCaseComboBox = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.addElement("--- Select a Demo Scenario ---");
        defaultComboBoxModel1.addElement("Demo Scenario 1");
        defaultComboBoxModel1.addElement("Demo Scenario 2");
        useCaseComboBox.setModel(defaultComboBoxModel1);
        statusBarPanel.add(useCaseComboBox, new GridConstraints(0, 4, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 10, 1, 10), -1, -1));
        statusBarPanel.add(panel2, new GridConstraints(3, 3, 1, 7, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
        statusBarLabel = new JLabel();
        statusBarLabel.setEnabled(false);
        statusBarLabel.setHorizontalAlignment(11);
        statusBarLabel.setText("Please load shapefiles...");
        panel2.add(statusBarLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 10, 1, 10), -1, -1));
        statusBarPanel.add(panel3, new GridConstraints(3, 10, 1, 29, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
        daysElapsedLabel = new JLabel();
        daysElapsedLabel.setText("Days Elapsed: ");
        panel3.add(daysElapsedLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        planCostLabel = new JLabel();
        planCostLabel.setHorizontalAlignment(0);
        planCostLabel.setHorizontalTextPosition(0);
        planCostLabel.setText("Total km driven:");
        panel3.add(planCostLabel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        resetButton = new JButton();
        resetButton.setText("Reset");
        statusBarPanel.add(resetButton, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, 1, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        continuePlanningCheckBox = new JCheckBox();
        continuePlanningCheckBox.setText("Continue Planning ");
        statusBarPanel.add(continuePlanningCheckBox, new GridConstraints(1, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        simulationModeCheckbox = new JCheckBox();
        simulationModeCheckbox.setEnabled(true);
        simulationModeCheckbox.setSelected(false);
        simulationModeCheckbox.setText("Simulation Mode");
        statusBarPanel.add(simulationModeCheckbox, new GridConstraints(1, 5, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        label6.setText("Stop After (Days)");
        statusBarPanel.add(label6, new GridConstraints(0, 6, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        stopSpinner = new JSpinner();
        statusBarPanel.add(stopSpinner, new GridConstraints(0, 7, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        label7.setText("Minimum Fullness");
        statusBarPanel.add(label7, new GridConstraints(1, 6, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        wasteBinMinSpinner = new JSpinner();
        statusBarPanel.add(wasteBinMinSpinner, new GridConstraints(1, 7, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        label8.setText("%");
        statusBarPanel.add(label8, new GridConstraints(1, 8, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel1.add(mapPane, BorderLayout.CENTER);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
