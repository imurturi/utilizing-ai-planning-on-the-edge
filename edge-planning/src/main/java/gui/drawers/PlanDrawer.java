package gui.drawers;

import gui.components.JViennaMapPane;
import models.*;
import models.actions.Action;
import models.actions.BinEmptyAction;
import models.actions.MoveAction;
import models.actions.TruckEmptyAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.identity.FeatureIdImpl;
import org.geotools.map.FeatureLayer;
import org.geotools.styling.*;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;
import util.ParameterStore;
import util.UseCaseObserver;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class PlanDrawer {

	private final SimpleFeatureSource nodeFeatureSource;
	private JViennaMapPane mapPane;
	private HashMap<EdgeDevice, ArrayList<Plan>> plans;
	private static Logger LOG = LogManager.getLogger(PlanDrawer.class);

	public PlanDrawer(JViennaMapPane mapPane, SimpleFeatureSource nodeFeatureSource) {
		this.mapPane = mapPane;
		this.nodeFeatureSource = nodeFeatureSource;
	}

	private void drawRoutes() {
		if (plans != null && !plans.isEmpty()) {
			StyleFactory sf = CommonFactoryFinder.getStyleFactory();
			Style style = sf.createStyle();
			plans.forEach((edgeDevice, plans) -> {
				plans.forEach(plan -> {
					FeatureTypeStyle fts = getFeatureTypeStyleForVehicles(plan.getVehicles().values());
					style.featureTypeStyles().add(fts);
				});
			});
			((FeatureLayer) mapPane.getMapContent().layers().get(0)).setStyle(style);
		}

	}

	public boolean step(JLabel statusLabel) {
		StringBuffer stepText = null;
		boolean anyStepsLeft = false;
		boolean routeChanged = false;
		int minFullness = ParameterStore.getInstance().getMinFullness();
		boolean simulationMode = ParameterStore.getInstance().isSimulationMode();
		float simulationStepTime = ParameterStore.getInstance().getSimulationStepSpeed();
		UseCaseObserver useCaseObserver = UseCaseObserver.getInstance();
		if (plans != null && !plans.isEmpty()) {
			synchronized (plans) {
				List<Plan> plansToRemove = new ArrayList<>();
				for (EdgeDevice edgeDevice : plans.keySet()) {
					ArrayList<Plan> currentPlans = plans.get(edgeDevice);
					GeneratedCity generatedCity = edgeDevice.getGeneratedCity();
					for (Plan currentPlan : currentPlans) {
						if (!currentPlan.getActions().isEmpty()) {
							Action nextAction = currentPlan.getActions().getFirst();
							ArrayList<Vehicle> storedVehicles = edgeDevice.getLeasedVehicles();
							LinkedList<WasteBin> wasteBins = edgeDevice.getWasteBins();
							int truckCapacity = ParameterStore.getInstance().getVehicleCapacity();
							int wasteBinCapacity = ParameterStore.getInstance().getBinCapacity();

							switch (nextAction.getActionType()) {
								case MOVE:
									MoveAction nextMoveAction = (MoveAction) nextAction;
									Vehicle vehicleToMove = currentPlan.getVehicles().get(nextMoveAction.getVehicle().getId());
									vehicleToMove.getRoute().remove(((MoveAction) nextAction).getStreet());
									currentPlan.getVehicles().put(vehicleToMove.getId(), vehicleToMove);
									vehicleToMove.setAtNodeId(nextMoveAction.getToNode());
									Coordinate newCoordinates = findCoordinates(nextMoveAction.getToNode());
									vehicleToMove.setAtCoordinate(newCoordinates);
									useCaseObserver.increaseTotalPlanExectionTime(simulationStepTime);


/*							if (storedVehicles != null) {
								storedVehicles.add(vehicleToMove.getId(), vehicleToMove);
							}*/
									routeChanged = true;
									currentPlan.getActions().removeFirst();
									if (stepText == null)
										stepText = new StringBuffer(vehicleToMove.getId() + " moving along " + getStreetName(nextMoveAction.getStreet(), generatedCity));
									else
										stepText.append(" | ").append(vehicleToMove.getId() + " moving along " + getStreetName(nextMoveAction.getStreet(), generatedCity));
									break;

								case BINEMPTY:
									BinEmptyAction emptyAction = (BinEmptyAction) nextAction;

									wasteBins.forEach(wasteBin -> {
										if (wasteBin.getId().equals(emptyAction.getBin())) {
											if (storedVehicles != null) {
												Vehicle emptyingVehicle = emptyAction.getVehicle();
												LOG.info("{}: Emptying Waste Bin {}", emptyingVehicle.getId(), wasteBin.getId());
												emptyingVehicle.setCapacity(emptyingVehicle.getCapacity() - (int) (wasteBinCapacity * wasteBin.getFullness() / 100));
//										storedVehicles.put(emptyingVehicle.getId(), emptyingVehicle);
											}


											edgeDevice.emptyBin(wasteBin);

										}
									});
									currentPlan.getActions().removeFirst();
									if (stepText == null)
										stepText = new StringBuffer(emptyAction.getVehicle().getId() + " emptying " + emptyAction.getBin() + " at " + getNodeName(emptyAction.getAtNode(), generatedCity));
									else
										stepText.append(" | ").append(emptyAction.getVehicle().getId() + " emptying " + emptyAction.getBin() + " at " + getNodeName(emptyAction.getAtNode(), generatedCity));
									break;
								case TRUCKEMPTY:
									TruckEmptyAction truckEmptyAction = (TruckEmptyAction) nextAction;

									if (storedVehicles != null) {
										Vehicle emptyingVehicle = truckEmptyAction.getVehicle();
										emptyingVehicle.setCapacity(truckCapacity);
//								storedVehicles.put(emptyingVehicle.getId(), emptyingVehicle);
									}

									currentPlan.getActions().removeFirst();
									if (stepText == null)
										stepText = new StringBuffer("Emptying " + truckEmptyAction.getVehicle().getId() + " at " + truckEmptyAction.getPlant());
									else
										stepText.append(" | ").append("Emptying " + truckEmptyAction.getVehicle().getId() + " at " + truckEmptyAction.getPlant());
									break;
							}


//					ParameterStore.getInstance().setVehicleList(storedVehicles);
							edgeDevice.setWasteBins(wasteBins);
							anyStepsLeft = true;


						} else {
							plansToRemove.add(currentPlan);
							if (statusLabel != null)
								stepText = new StringBuffer("Plan finished.");
							if (!edgeDevice.isWorkToBeDone(minFullness) && edgeDevice.getLeasedVehicles() != null && !edgeDevice.getLeasedVehicles().isEmpty()) {
								edgeDevice.getLeasedVehicles().removeAll(currentPlan.getVehicles().values());
//						ParameterStore.getInstance().getUnassignedVehicles().removeAll(currentPlan.getVehicles().values());
								ParameterStore.getInstance().getUnassignedVehicles().addAll(currentPlan.getVehicles().values());
							}

						}
					}


					if (!plansToRemove.isEmpty()) {
						currentPlans.removeAll(plansToRemove);
						plans.put(edgeDevice, currentPlans);
						edgeDevice.setCurrentPlans(currentPlans);
					}
				}

				if (statusLabel != null && stepText != null) {
					if (stepText.length() > 128) {
						String fixedText = stepText.substring(0, 128) + "...";
						statusLabel.setText(fixedText);
					} else {
						statusLabel.setText(stepText.toString());
					}
				} else if (statusLabel != null && stepText == null)
					statusLabel.setText("Plan finished.");
				if (routeChanged && !simulationMode)
					drawRoutes();

				return anyStepsLeft;
			}
		}
		return false;
	}

	private Coordinate findCoordinates(Double toNode) {

		FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

		Filter nodeCoordinateFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(toNode));
		Coordinate coordinates = null;

		try {
			SimpleFeatureCollection edgeNodeCandidates = nodeFeatureSource.getFeatures(nodeCoordinateFilter);
			if (edgeNodeCandidates.size() == 1) {
				SimpleFeatureIterator iterator = edgeNodeCandidates.features();
				while (iterator.hasNext()) {
					SimpleFeature targetNode = iterator.next();
					Geometry geom = (Geometry) targetNode.getDefaultGeometry();
					org.locationtech.jts.geom.Point nodePositionCenter = (org.locationtech.jts.geom.Point) geom.getGeometryN(0);
					coordinates = nodePositionCenter.getCoordinate();
				}
				iterator.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}


		return coordinates;
	}

	private String getStreetName(String streetID, GeneratedCity generatedCity) {
		if (generatedCity != null)
			return generatedCity.getStreetNameMap().get(streetID);
		return streetID;
	}

	private String getNodeName(Double nodeID, GeneratedCity generatedCity) {
		if (generatedCity != null)
			return generatedCity.getNodeNameMap().get(nodeID);
		return String.valueOf(nodeID);
	}

	private FeatureTypeStyle getFeatureTypeStyleForVehicles(Collection<Vehicle> vehicles) {
		FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2();
		StyleFactory sf = CommonFactoryFinder.getStyleFactory();

		FeatureTypeStyle fts = sf.createFeatureTypeStyle();

		HashMap<FeatureId, Color> streetColorMap = new HashMap<>();
		HashMap<FeatureId, Color> duplicateColorMap = new HashMap<>();

		for (Vehicle vehicle : vehicles) {
			Color vehicleColor = vehicle.getColor();

			Set<FeatureId> streetFeatureIds = generateFeatureIDList(vehicle.getRoute());
			streetFeatureIds.forEach(featureId -> {
				if (streetColorMap.containsKey(featureId)) {
					Color oldColor = streetColorMap.get(featureId);

					if (duplicateColorMap.containsKey(featureId)) {
						oldColor = duplicateColorMap.get(featureId);
					}

					int newRed = (int) ((oldColor.getRed() + vehicleColor.getRed()) / 2);
					int newGreen = (int) ((oldColor.getGreen() + vehicleColor.getGreen()) / 2);
					int newBlue = (int) ((oldColor.getBlue() + vehicleColor.getBlue()) / 2);

					Color newColor = new Color(newRed, newGreen, newBlue);

					duplicateColorMap.put(featureId, newColor);
				} else {
					streetColorMap.put(featureId, vehicleColor);
				}
			});


			org.geotools.styling.Stroke stroke = sf.createStroke(filterFactory.literal(vehicle.getColor()), filterFactory.literal(4));
			LineSymbolizer symbolizer = sf.createLineSymbolizer(stroke, "the_geom");


			Rule rule = sf.createRule();
			rule.symbolizers().add(symbolizer);
			rule.setFilter(filterFactory.id(streetFeatureIds));


			fts.rules().add(rule);
		}

		duplicateColorMap.forEach((featureId, color) -> {
			org.geotools.styling.Stroke stroke = sf.createStroke(filterFactory.literal(color), filterFactory.literal(4));
			LineSymbolizer symbolizer = sf.createLineSymbolizer(stroke, "the_geom");


			Rule rule = sf.createRule();
			rule.symbolizers().add(symbolizer);
			rule.setFilter(filterFactory.id(featureId));
			fts.rules().add(rule);
		});

/*		Stroke otherStroke = sf.createStroke(filterFactory.literal(Color.BLACK), filterFactory.literal(1));
		LineSymbolizer otherSymbol = sf.createLineSymbolizer(otherStroke, "the_geom");
		Rule otherRule = sf.createRule();
		otherRule.symbolizers().add(otherSymbol);
		otherRule.setFilter(filterFactory.id(streetColorMap.keySet()));
		otherRule.setElseFilter(true);
		fts.rules().add(otherRule);*/
		return fts;

	}

	private Set<FeatureId> generateFeatureIDList(LinkedList<String> route) {
		Set<FeatureId> featureIds = new HashSet<>();
		route.forEach(step -> featureIds.add(new FeatureIdImpl(step)));

		return featureIds;
	}

	public void addPlan(EdgeDevice edgeDevice, Plan plan) {
		if (plans == null)
			plans = new HashMap<>();

		boolean simulationMode = ParameterStore.getInstance().isSimulationMode();

		synchronized (plans) {
			ArrayList<Plan> currentPlans = plans.get(edgeDevice);
			if(currentPlans == null)
				currentPlans = new ArrayList<>();
			currentPlans.add(plan);
			edgeDevice.setCurrentPlans(currentPlans);
			plans.put(edgeDevice, currentPlans);
			if(!simulationMode)
				drawRoutes();
		}
	}

	public void resetPlans() {
		synchronized (plans){
			plans = new HashMap<>();
		}
	}
}
