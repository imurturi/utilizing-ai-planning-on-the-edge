package gui.drawers;

import gui.components.JViennaMapPane;
import models.Vehicle;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.core.animation.timing.TimingSource;
import org.jdesktop.core.animation.timing.interpolators.AccelerationInterpolator;
import org.jdesktop.swing.animation.timing.sources.SwingTimerTimingSource;
import org.locationtech.jts.geom.Coordinate;
import org.opengis.filter.FilterFactory2;
import util.ParameterStore;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Collection;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class VehicleDrawer {

	private JViennaMapPane mapPane;
	public VehicleDrawer(JViennaMapPane mapPane) {
		this.mapPane = mapPane;
	}

	public void drawVehicles(Collection<Vehicle> vehicles, Graphics2D graphics2D) {
		AffineTransform tr = mapPane.getWorldToScreenTransform();
		int animationStepSpeedInMS = ParameterStore.getInstance().getAnimationSpeed();

		for (Vehicle vehicle : vehicles) {
			Coordinate nodePositionCenter = vehicle.getAtCoordinate();

			Point2D nodePosition = new Point2D.Double(nodePositionCenter.getX(), nodePositionCenter.getY());
			Point nodeScreenPosition = new Point((int) nodePositionCenter.getX(), (int) nodePositionCenter.getY());

			tr.transform(nodePosition, nodeScreenPosition);

			if (vehicle.getScreenPosition() == null || vehicle.getScreenPosition().equals(nodeScreenPosition) ) {
				vehicle.setScreenPosition(nodeScreenPosition);
				vehicle.draw(graphics2D);
				mapPane.paintVehicles();
			} else {
				Animator vehicleAnimator = vehicle.getAnimator();
				if (vehicleAnimator != null)
					vehicleAnimator.stop();
				SwingTimerTimingSource animationTimer = new SwingTimerTimingSource();

				animationTimer.init();
				animationTimer.addPostTickListener((source, nanoTime) -> {
					vehicle.draw(graphics2D);
					mapPane.repaintVehicle(vehicle.getRectanglePosition());
				});

				animationTimer.addTickListener((timingSource, l) -> {
					vehicle.clear(graphics2D);
					mapPane.repaint(vehicle.getRectanglePosition(), false);
				});

				vehicleAnimator = new Animator.Builder(animationTimer).setDuration(animationStepSpeedInMS/2, MILLISECONDS).setDisposeTimingSource(true).build();

				vehicle.setAnimator(vehicleAnimator);
				vehicleAnimator.addTarget(PropertySetter
						.getTargetTo(vehicle, "screenPosition", new AccelerationInterpolator(0.5, 0.5), nodeScreenPosition));
				vehicleAnimator.start();
			}

		}

	}

}
