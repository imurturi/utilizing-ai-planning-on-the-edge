package gui.drawers;

import gui.components.JViennaMapPane;
import models.WasteBin;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class BinGraphicDrawer {

	private JViennaMapPane mapPane;
	private SimpleFeatureSource nodeFeatureSource;

	private final Image BIN_IMAGE;

	private HashMap<String, Float> binStatusMap;
	private HashMap<String, Point> binPositionAreaMap;

	private boolean viewPortChanged = false;

	public BinGraphicDrawer(JViennaMapPane mapPane, SimpleFeatureSource nodeFeatureSource) {
		this.mapPane = mapPane;
		this.nodeFeatureSource = nodeFeatureSource;
		this.binStatusMap = new HashMap<>();
		binPositionAreaMap = new HashMap<>();
		this.BIN_IMAGE = new ImageIcon(getClass().getResource("/images/wastebin_square.png")).getImage();
	}

	public boolean drawBins(List<WasteBin> bins, Graphics2D graphics2D) {
		final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
		AffineTransform tr = mapPane.getWorldToScreenTransform();

		if (viewPortChanged) {
			binPositionAreaMap = new HashMap<>();
			binStatusMap = new HashMap<>();
		}

		List<WasteBin> binsToReDraw = null;
		if (viewPortChanged)
			binsToReDraw = bins;
		else
			binsToReDraw = bins.parallelStream().filter(wasteBin -> !binStatusMap.containsKey(wasteBin.getId()) || binStatusMap.get(wasteBin.getId()) != wasteBin.getFullness()).collect(Collectors.toList());

		if (!binsToReDraw.isEmpty()) {
			for (WasteBin bin : binsToReDraw) {
				Point nodeScreenPosition = null;
				if (binPositionAreaMap.containsKey(bin.getId())) {
					nodeScreenPosition = binPositionAreaMap.get(bin.getId());
				} else {
					Coordinate nodeWorldPosition = bin.getNodeWorldPositon();
					if(nodeWorldPosition == null) {
						try {
							Filter nodeFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(bin.getAtNode()));
							FeatureCollection<SimpleFeatureType, SimpleFeature> filteredFeatures = nodeFeatureSource.getFeatures(nodeFilter);

							if (filteredFeatures.size() == 1) {
								FeatureIterator<SimpleFeature> featureIterator = filteredFeatures.features();
								SimpleFeature node = featureIterator.next();
								featureIterator.close();
								Geometry geom = (Geometry) node.getDefaultGeometry();

								org.locationtech.jts.geom.Point nodePositionCenter = (org.locationtech.jts.geom.Point) geom.getGeometryN(0);

								nodeWorldPosition = nodePositionCenter.getCoordinate();
								bin.setNodeWorldPositon(nodeWorldPosition);

								Point2D nodePosition = new Point2D.Double(nodePositionCenter.getX(), nodePositionCenter.getY());

								nodeScreenPosition = new Point((int) nodePositionCenter.getX(), (int) nodePositionCenter.getY());


								tr.transform(nodePosition, nodeScreenPosition);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {

						Point2D nodePosition = new Point2D.Double(nodeWorldPosition.getX(), nodeWorldPosition.getY());
						nodeScreenPosition = new Point();
						tr.transform(nodePosition, nodeScreenPosition);
					}
				}

				graphics2D.setStroke(new BasicStroke(3));
				if (bin.getFullness() < 100)
					graphics2D.setColor(Color.GREEN);
				else
					graphics2D.setColor(Color.RED);

				double mapHeight = mapPane.getBounds().getHeight();

				int height = (int) (mapHeight * 0.016);
				int width = height;

				mapPane.getFeatureImageGraphics().clearRect(nodeScreenPosition.x, nodeScreenPosition.y, width, height);
				mapPane.repaint(new Rectangle(nodeScreenPosition.x, nodeScreenPosition.y, width, height), false);

				graphics2D.fillRect(nodeScreenPosition.x + (int) (width * 0.07), nodeScreenPosition.y + (int) (height * 0.13), (int) (width * 0.86), height - (int) (height * 0.13));
				graphics2D.setBackground(new Color(0, 0, 0, 0));
				graphics2D.clearRect(nodeScreenPosition.x + (int) (width * 0.07), nodeScreenPosition.y + (int) (height * 0.13), (int) (width * 0.86), (int) (height - (height - height * 0.13) * bin.getFullness() / 100));



				graphics2D.drawImage(BIN_IMAGE, nodeScreenPosition.x, nodeScreenPosition.y, width, height, null);
				binStatusMap.put(bin.getId(), bin.getFullness());
				binPositionAreaMap.put(bin.getId(), new Point(nodeScreenPosition.x, nodeScreenPosition.y));
			}
			viewPortChanged = false;
			return true;
		}

		return false;

	}

	public void setViewPortChanged(boolean viewPortChanged) {
		this.viewPortChanged = viewPortChanged;
	}
}


