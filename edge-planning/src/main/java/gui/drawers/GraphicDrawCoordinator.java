package gui.drawers;

import gui.components.JViennaMapPane;
import models.*;
import org.geotools.data.simple.SimpleFeatureSource;
import util.ParameterStore;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GraphicDrawCoordinator {

	private BinGraphicDrawer binGraphicDrawer;
	private PlanDrawer planDrawer;
	private VehicleDrawer vehicleDrawer;
	private EdgeDeviceDrawer edgeDeviceDrawer;

	private JViennaMapPane mapPane;
	private JLabel statusLabel;

	public GraphicDrawCoordinator(JViennaMapPane mapPane, SimpleFeatureSource nodeFeatureSource, JLabel statusLabel) {
		this.mapPane = mapPane;
		this.binGraphicDrawer = new BinGraphicDrawer(mapPane, nodeFeatureSource);
		this.planDrawer = new PlanDrawer(mapPane, nodeFeatureSource);
		this.vehicleDrawer = new VehicleDrawer(mapPane);
		this.edgeDeviceDrawer = new EdgeDeviceDrawer(mapPane, nodeFeatureSource);
		this.statusLabel = statusLabel;
	}

	public void drawBins(LinkedList<WasteBin> binList, Graphics2D graphics2D) {
		if (graphics2D == null)
			graphics2D = (Graphics2D) mapPane.getFeatureImageGraphics();
		LinkedList<WasteBin> bins = binList;
		if (bins == null)
			bins = ParameterStore.getInstance().getBinList();
		if (bins != null) {
			if(binGraphicDrawer.drawBins(bins, graphics2D))
				mapPane.paintFeatures();
		}
	}

	synchronized public void drawPlan(EdgeDevice edgeDevice, Plan parsedPlan) {
		if (statusLabel != null)
			statusLabel.setText("Plan found with Length: " + parsedPlan.getCost() + "m in " + parsedPlan.getPlanDuration() / 1000 + "s.");
		planDrawer.addPlan(edgeDevice, parsedPlan);
	}

	public void drawVehicles(ArrayList<Vehicle> currentVehicles, Graphics2D graphics2D) {
		if (graphics2D == null)
			graphics2D = (Graphics2D) mapPane.getVehicleImageGraphics();
		ArrayList<Vehicle> vehicles = currentVehicles;
		if (vehicles == null)
			vehicles = ParameterStore.getInstance().getAllVehicleList();
		if (vehicles != null)
			vehicleDrawer.drawVehicles(vehicles, graphics2D);
	}

	public void drawEdgeDevices(List<EdgeDevice> edgeDevices, Graphics2D graphics2D) {
		if (graphics2D == null)
			graphics2D = (Graphics2D) mapPane.getFeatureImageGraphics();
		List<EdgeDevice> edges = edgeDevices;
		if (edges == null)
			edges = ParameterStore.getInstance().getEdgeDevices();
		if (edges != null)
			if(edgeDeviceDrawer.drawEdges(edges, graphics2D))
				mapPane.paintFeatures();
	}

	public boolean drawNextStep(boolean simulationMode) {
		boolean stepsLeft = planDrawer.step(statusLabel);
		if (!stepsLeft && !simulationMode) {
			drawBins(null, null);
			drawEdgeDevices(null, null);
			drawVehicles(null, null);
		}
		return stepsLeft;
	}

	public void setViewPortChanged(boolean viewPortChanged) {
		binGraphicDrawer.setViewPortChanged(viewPortChanged);
		edgeDeviceDrawer.setViewPortChanged(viewPortChanged);
	}

	public void resetPlans(){
		planDrawer.resetPlans();
	}
}
