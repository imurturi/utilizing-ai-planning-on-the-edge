package gui.drawers;

import gui.components.JViennaMapPane;
import models.EdgeDevice;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.locationtech.jts.awt.ShapeWriter;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;

public class EdgeDeviceDrawer {

	private JViennaMapPane mapPane;
	private SimpleFeatureSource nodeFeatureSource;

	private final Image EDGE_IMAGE;

	private boolean viewPortChanged = true;

	public EdgeDeviceDrawer(JViennaMapPane mapPane, SimpleFeatureSource nodeFeatureSource) {
		this.mapPane = mapPane;
		this.nodeFeatureSource = nodeFeatureSource;
		this.EDGE_IMAGE = new ImageIcon(getClass().getResource("/images/edge_mist.png")).getImage();
	}

	public boolean drawEdges(List<EdgeDevice> edgeDevices, Graphics2D graphics2D) {
		final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
		AffineTransform tr = mapPane.getWorldToScreenTransform();

		if (viewPortChanged) {
			synchronized (edgeDevices) {
				for (EdgeDevice edgeDevice : edgeDevices) {
					Point nodeScreenPosition = new Point();
					Point2D.Double nodePositionCenter = null;
					Coordinate edgeCoordinate = edgeDevice.getNodeCoordinate();
					if (edgeCoordinate != null) {

						nodePositionCenter = new Point2D.Double(edgeCoordinate.x, edgeCoordinate.y);
						tr.transform(nodePositionCenter, nodeScreenPosition);
					} else {
						try {
							Filter nodeFilter = ff.equals(ff.property("GIP_OBJECT"), ff.literal(edgeDevice.getAtNodeId()));
							FeatureCollection<SimpleFeatureType, SimpleFeature> filteredFeatures = nodeFeatureSource.getFeatures(nodeFilter);

							if (filteredFeatures.size() == 1) {
								FeatureIterator<SimpleFeature> featureIterator = filteredFeatures.features();
								SimpleFeature node = featureIterator.next();
								Geometry geom = (Geometry) node.getDefaultGeometry();

								org.locationtech.jts.geom.Point geometry = ((org.locationtech.jts.geom.Point) geom.getGeometryN(0));
								edgeCoordinate = geometry.getCoordinate();

								nodePositionCenter = new Point2D.Double(geometry.getX(), geometry.getY());
								tr.transform(nodePositionCenter, nodeScreenPosition);

								featureIterator.close();
							}

						} catch (IOException e) {
							e.printStackTrace();
						}
					}


					int width = EDGE_IMAGE.getWidth(null);
					int height = EDGE_IMAGE.getHeight(null);

					if (nodeScreenPosition != null) {
						double diameterInMeters = edgeDevice.getAuthorityRadius() * 2 + 200;
						GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
						shapeFactory.setNumPoints(64); // adjustable
						shapeFactory.setCentre(edgeCoordinate);
						// Length in meters of 1° of latitude = always 111.32 km
						shapeFactory.setWidth(diameterInMeters / 111320d);
						// Length in meters of 1° of longitude = 40075 km * cos( latitude ) / 360
						shapeFactory.setHeight(diameterInMeters / (40075000 * Math.cos(Math.toRadians(nodePositionCenter.getX())) / 360));

						org.locationtech.jts.geom.Polygon circle = shapeFactory.createEllipse();
						ShapeWriter sw = new ShapeWriter();
						Shape circleShape = sw.toShape(circle);
						Shape transformedShape = tr.createTransformedShape(circleShape);

						graphics2D.drawImage(EDGE_IMAGE, nodeScreenPosition.x - width / 2, nodeScreenPosition.y - height / 2, width, height, null);
						graphics2D.setColor(Color.RED);
						graphics2D.setStroke(new BasicStroke(4));
						graphics2D.setFont(new Font("Roboto", Font.BOLD, 24));
						graphics2D.drawString(edgeDevice.getSimulationID(), nodeScreenPosition.x - width / 2, transformedShape.getBounds().y + 36);

						graphics2D.draw(transformedShape);
					}

				}
			}

			viewPortChanged = false;
			return true;
		}
		return false;
	}

	public void setViewPortChanged(boolean viewPortChanged) {
		this.viewPortChanged = viewPortChanged;
	}
}
