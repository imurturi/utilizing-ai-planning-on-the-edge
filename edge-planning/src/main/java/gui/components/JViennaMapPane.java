package gui.components;

import gui.drawers.GraphicDrawCoordinator;
import org.geotools.map.MapContent;
import org.geotools.renderer.GTRenderer;
import org.geotools.swing.*;
import org.geotools.swing.event.MapPaneEvent;
import org.geotools.swing.event.MapPaneListener;

import java.awt.*;
import java.awt.image.BufferedImage;

public class JViennaMapPane extends AbstractMapPane {
	private MapContent baseContent;
	private GraphicDrawCoordinator drawCoordinator;
	private RenderingExecutor executor;
	private GTRenderer renderer;
	private boolean viewPortChanged = true;
	private BufferedImage baseImage;
	private Graphics2D baseImageGraphics;
	private BufferedImage routeImage;
	private BufferedImage featureImage;
	private Graphics2D featureImageGraphics;
	private BufferedImage vehicleImage;
	private Graphics2D vehicleImageGraphics;

	private BufferedImage backBuffer;
	private Graphics2D backBufferGraphics;

	public JViennaMapPane(RenderingExecutor executor, GTRenderer renderer) {
		super(null, executor);
		this.renderer = renderer;
		this.executor = executor;
	}

	public void setDrawCoordinator(GraphicDrawCoordinator drawCoordinator) {
		this.drawCoordinator = drawCoordinator;
		addMapPaneListener(mapPaneListener);
	}

	private MapPaneListener mapPaneListener = new MapPaneListener() {
		@Override
		public void onNewMapContent(MapPaneEvent ev) {
			if (drawCoordinator != null)
				drawCoordinator.setViewPortChanged(true);
			viewPortChanged = true;
		}

		@Override
		public void onDisplayAreaChanged(MapPaneEvent ev) {
			if (drawCoordinator != null)
				drawCoordinator.setViewPortChanged(true);
			viewPortChanged = true;
		}

		@Override
		public void onRenderingStarted(MapPaneEvent ev) {
		}

		@Override
		public void onRenderingStopped(MapPaneEvent ev) {
		}
	};

	@Override
	protected void drawLayers(boolean recreate) {
		drawingLock.tryLock();
		Graphics2D g2 = (Graphics2D) getGraphics();

		if (viewPortChanged || baseImage == null || recreate) {
			/* First Load / Viewport Changed */
			if (baseContent != null) {
				Rectangle r = mapContent.getViewport().getScreenArea();
				baseContent.setViewport(mapContent.getViewport());
				if (baseImage == null || baseImageGraphics == null) {
					baseImage =
							GraphicsEnvironment.getLocalGraphicsEnvironment()
									.getDefaultScreenDevice()
									.getDefaultConfiguration()
									.createCompatibleImage(
											r.width, r.height, Transparency.TRANSLUCENT);

					featureImage =
							GraphicsEnvironment.getLocalGraphicsEnvironment()
									.getDefaultScreenDevice()
									.getDefaultConfiguration()
									.createCompatibleImage(
											r.width, r.height, Transparency.TRANSLUCENT);
					vehicleImage =
							GraphicsEnvironment.getLocalGraphicsEnvironment()
									.getDefaultScreenDevice()
									.getDefaultConfiguration()
									.createCompatibleImage(
											r.width, r.height, Transparency.TRANSLUCENT);

					backBuffer =
							GraphicsEnvironment.getLocalGraphicsEnvironment()
									.getDefaultScreenDevice()
									.getDefaultConfiguration()
									.createCompatibleImage(
											r.width, r.height, Transparency.TRANSLUCENT);

					if (baseImageGraphics != null) {
						baseImageGraphics.dispose();
					}
					if (featureImageGraphics != null)
						featureImageGraphics.dispose();
					if (vehicleImageGraphics != null)
						vehicleImageGraphics.dispose();
					if(backBufferGraphics != null)
						backBufferGraphics.dispose();

					baseImageGraphics = baseImage.createGraphics();
					featureImageGraphics = featureImage.createGraphics();
					vehicleImageGraphics = vehicleImage.createGraphics();
					backBufferGraphics = backBuffer.createGraphics();
				}
				baseImageGraphics.setBackground(getBackground());
				baseImageGraphics.clearRect(0, 0, r.width, r.height);
				featureImageGraphics.setBackground(new Color(0, 0, 0, 0));
				featureImageGraphics.clearRect(0, 0, r.width, r.height);
				vehicleImageGraphics.setBackground(new Color(0, 0, 0, 0));
				vehicleImageGraphics.clearRect(0, 0, r.width, r.height);
				backBufferGraphics.setBackground(new Color(0, 0, 0, 0));
				backBufferGraphics.clearRect(0, 0, r.width, r.height);

				renderer.setMapContent(baseContent);

				executor.submit(baseContent, renderer, baseImageGraphics, new RenderingExecutorListener() {
					@Override
					public void onRenderingStarted(RenderingExecutorEvent ev) {

					}

					@Override
					public void onRenderingCompleted(RenderingExecutorEvent ev) {
						renderRoute(g2);
					}

					@Override
					public void onRenderingFailed(RenderingExecutorEvent ev) {

					}
				});

				viewPortChanged = false;
			}

		} else {
			renderRoute(g2);
		}


		drawingLock.unlock();

	}

	public void repaint(Rectangle r, boolean update) {
		if (getVisibleRect().contains(r)) {
			backBufferGraphics.drawImage(baseImage.getSubimage(r.x, r.y, r.width, r.height), r.x, r.y, null);
			backBufferGraphics.drawImage(routeImage.getSubimage(r.x, r.y, r.width, r.height), r.x, r.y, null);
			backBufferGraphics.drawImage(featureImage.getSubimage(r.x, r.y, r.width, r.height), r.x, r.y, null);
			if(update)
				update(getGraphics());
		}
	}

	@Override
	public void repaint(Rectangle r) {
		repaint(r, true);
	}

	private void renderRoute(Graphics2D g2) {
		if (mapContent != null) {
			Rectangle r = mapContent.getViewport().getScreenArea();
			if(routeImage == null) {
				routeImage = GraphicsEnvironment.getLocalGraphicsEnvironment()
						.getDefaultScreenDevice()
						.getDefaultConfiguration()
						.createCompatibleImage(
								r.width, r.height, Transparency.TRANSLUCENT);
			} else {
				Graphics2D routeGraphics = (Graphics2D) routeImage.getGraphics();
				routeGraphics.setBackground(new Color(255, 255, 255, 0));
				Rectangle screen = getVisibleRect();
				routeGraphics.clearRect(0,0, (int)screen.getWidth(), (int)screen.getHeight());
			}
			Graphics2D routeGraphic = routeImage.createGraphics();

			renderer.setMapContent(mapContent);
			executor.submit(mapContent, renderer, routeGraphic, new RenderingExecutorListener() {
				@Override
				public void onRenderingStarted(RenderingExecutorEvent ev) {
				}

				@Override
				public void onRenderingCompleted(RenderingExecutorEvent ev) {
					if (drawCoordinator != null) {
						drawCoordinator.drawBins(null, featureImageGraphics);
						drawCoordinator.drawEdgeDevices(null, featureImageGraphics);
						drawCoordinator.drawVehicles(null, vehicleImageGraphics);
						backBufferGraphics.drawImage(baseImage, imageOrigin.x, imageOrigin.y, null);
						backBufferGraphics.drawImage(routeImage, imageOrigin.x, imageOrigin.y, null);
						backBufferGraphics.drawImage(vehicleImage, imageOrigin.x, imageOrigin.y, null);
						backBufferGraphics.drawImage(featureImage, imageOrigin.x, imageOrigin.y, null);

						drawCoordinator.setViewPortChanged(false);
						viewPortChanged = false;

						update(g2);
					}
				}

				@Override
				public void onRenderingFailed(RenderingExecutorEvent ev) {

				}
			});
		}

	}


	@Override
	public void paint(Graphics g) {
		if(backBuffer != null)
			g.drawImage(backBuffer,0,0,null);
		else
			super.paint(g);
	}

	public Graphics2D getVehicleImageGraphics() {
		return vehicleImageGraphics;
	}

	public Graphics2D getFeatureImageGraphics() {
		return featureImageGraphics;
	}

	public void setBaseContent(MapContent baseContent) {
		this.baseContent = baseContent;
		drawLayers(true);
	}

	public void repaintVehicle(Rectangle r) {
		if(backBufferGraphics != null && vehicleImage.getData().getBounds().contains(r))
			backBufferGraphics.drawImage(vehicleImage.getSubimage(r.x, r.y, r.width, r.height), r.x, r.y, null);
		update(getGraphics());
	}

	public void paintFeatures() {
		if(backBufferGraphics != null)
			backBufferGraphics.drawImage(featureImage, imageOrigin.x, imageOrigin.y, null);
		update(getGraphics());
	}

	public void paintVehicles() {
		if(backBufferGraphics != null)
			backBufferGraphics.drawImage(vehicleImage, imageOrigin.x, imageOrigin.y, null);
		update(getGraphics());
	}

	public void dispose() {
		drawLayers(true);
	}
}
