# Utilizing AI Planning on the Edge

1. I. Murturi, A. Egyed and S. Dustdar, "Utilizing AI Planning on the Edge," in IEEE Internet Computing, vol. 26, no. 2, pp. 28-35, 1 March-April 2022,doi: 10.1109/MIC.2021.3073434.

### Components

This project consists of two parts:

1. The Java 14 project for the planner / GUI (edge-planner)
2. A Python 3 based edge-device simulation (simulation)


### Building the Java Project from Source
- Check-out the repository
- Open a Terminal and build the Java Project with Maven3:
```sh
$ cd utilizing-ai-planning-on-the-edge
$ cd edge-planning
$ mvn clean install
```
This should download all the dependencies and compile the project.

### Running the Simulation
Before starting the Planner / GUI it is important to start the "Simulation Coordinator". This will allow to boot up web-servers on demand that will simulate the edge devices.
The simulation requires Python3 in order to run. If you do not have it installed on your system visit https://www.python.org/ and check the installation guide for your system.
To start the "Simulation Coordinator" type following:
```sh
$ cd utilizing-ai-planning-on-the-edge
$ cd simulation
$ python simulationCoordinator.py
```
Keep the web-server running and start the java project.

### Running the GUI / Planner
After building the java project from source you are able to run the executable JAR by doing the following:
```sh
$ cd utilizing-ai-planning-on-the-edge
$ cd edge-planning
$ cd target
$ java -jar edge-planning-1.0.jar
```
This should open the main GUI window.

### Using the GUI / Planner
##### Loading the Map
When the application is running you are able to load the default shapefiles (map data) by going to File -> Load Defaults
Once the map is loaded you can zoom in using the magnifying glass on the top right toolbar.
##### Loading Demo Scenarios
If you want a quick start then select a demo scenario from the drop-down. This will place a pre-configured number of devices and vehicles on the map.
##### Setting up an Edge-Device
- Check the simulation settings:
  - Bin Count
  - Bin Cargo Size
  - Vehicle Capacity
  - Simulation speed (Real time ms per simulation hour)
  - Edge Authority Radius
- Click the edge device tool ![ ](edge-device.png) on the bottom right.
- Place the edge device on a node (a dot at an intersection) on the map
- If the simulation coordinator is running a new server will be started and you can see the waste bins distributed on the map slowly filling up.
##### Adding Vehicles
- Select the vehicle placement tool ![ ](vehicle.png)
- Place the vehicle on a node on the map (note due to performance reasons there is a maximum distance a vehicle will be considered for a given edge device)

##### Starting the planning
- Configure the Planner settings on the bottom left (Note: the time values are relative to the simulation time you set in "Simulation Speed")
  - Step Speed (for Animation)
  - Plan Interval (for Automated Planning)
  - Minimum Fullnes of the Bins (the threshold to include in the current plan)
  - Continue Planning (keep on planning if there is still work to be done at the end of the execution of a plan, ignoring the plan interval)
  - Simulation Mode (disables all vehicle / route animations for faster simulation speeds)
- Start the planning execution in the set "Plan Interval" by pressing the "Start" button.

#Adam Egyed
