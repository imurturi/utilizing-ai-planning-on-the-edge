import json
import time
import sys
import threading
import urllib.parse
import urllib.request

from http.server import BaseHTTPRequestHandler, HTTPServer
from subprocess import Popen, PIPE
import os

# generate random integer values
from random import seed
from random import randint
from random import random
# seed random number generator
# seed(time.time())
seed(1)

HOST_NAME = 'localhost'
PORT_NUMBER = 12345

simulations = None

class SimulationCoordinator(BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "application/json")
        s.end_headers()
    def do_GET(s):
        global simulations
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "application/json")
        s.end_headers()
        if "/start" in s.path:
            o = urllib.parse.urlparse(s.path)
            queries = urllib.parse.parse_qs(o.query)
            simulationID = None;
            num_bins = 0
            if simulations == None:
                simulations = dict()
                simulationPort = PORT_NUMBER + 1;
            else:
                simulationPort = PORT_NUMBER + len(simulations) + 1;

            speed = 0.4
            for x in queries.get('num_bins', None):
                num_bins = int(x)
            for x in queries.get('simulationid', None):
                simulationID = x
            for x in queries.get('speed', None):
                speed = float(x)
            if simulationID not in simulations:
                print("Starting Simulation with Params[num_bins:%s speed:%s port:%s id:%s" % (str(num_bins), str(speed), str(simulationPort), str(simulationID)))
                process = Popen(['python', os.path.dirname(os.path.realpath(__file__))+'/simulation.py', str(num_bins), str(speed), str(simulationPort), str(simulationID)])
                simulationURL = "http://"+HOST_NAME + ":" + str(simulationPort)
                simulations[simulationID] = simulationURL
                s.wfile.write(simulationURL.encode())
            else:
                s.wfile.write((simulations[simulationID]).encode())
        if "/reset" in s.path and simulations != None:
            for x in simulations.values():
                with urllib.request.urlopen(x+"/stop") as response:
                    responseCode = response.read()
                    print(responseCode)
            simulations = None





def main():

    httpd = HTTPServer((HOST_NAME, PORT_NUMBER), SimulationCoordinator)
    print (time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()

    except KeyboardInterrupt:
        httpd.server_close()

    print (time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))

if __name__ == '__main__':
    main()
