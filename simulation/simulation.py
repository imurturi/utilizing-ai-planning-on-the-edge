import json
import time
import sys
import threading
import urllib.parse
from http.server import BaseHTTPRequestHandler, HTTPServer

# generate random integer values
from random import seed
from random import randint
from random import random
# seed random number generator
seed(time.time())
# seed(1)

HOST_NAME = 'localhost'
PORT_NUMBER = 12346
SIMULATION_ID = "e0"
httpd = None

test_mode = False

current_day = 1
current_hour = 0
day_length = 24

speed = 0.4
num_bins = 32
if len(sys.argv) - 1 > 0 :
    num_bins = int(sys.argv[1])
if len(sys.argv) - 1 > 1 :
    speed = float(sys.argv[2])/1000
if len(sys.argv) - 1 > 2 :
    PORT_NUMBER = int(sys.argv[3])
if len(sys.argv) - 1 > 3 :
    SIMULATION_ID = sys.argv[4]

num_bins_full = 0
full_percent = 80

min_distance = 10
max_distance = 1000

min_percent_h = 0.6 # 7 days to 100%
max_percent_h = 4.167 # 1 day to 100%
# min_percent_h = 0.27
# max_percent_h = 0.85

run_event = None;
thread = None;

bin_fullness = [0 for x in range(num_bins)]
bin_modifier = [pow(random(),3) for x in range(num_bins)] # assign value 0-1 to each bin

state = {
    'num_bins' : num_bins,
    'bin_fullness' : bin_fullness,
    'current_hour' : current_hour,
    'current_day'  : current_day
}

def initialize():
    global num_bins, bin_fullness, bin_modifier, num_bins_full, current_day, current_hour, state
    if test_mode == True:
        bin_fullness = [100 for x in range(num_bins)]
    else:
        bin_fullness = [0 for x in range(num_bins)]
    bin_modifier = [pow(random(),3) for x in range(num_bins)]
    num_bins_full = 0
    current_day = 1
    current_hour = 0
    state = {
        'num_bins' : num_bins,
        'bin_fullness' : bin_fullness,
        'current_hour' : current_hour,
        'current_day'  : current_day
    }
    print("--- Initializing with " +str(num_bins) + " bins ---")
    print("--- Speed: "+str(speed)+" ---")

def simulation(name, run_evento):
    global current_hour, current_day, state, num_bins, num_bins_full, run_event
    print("--- Current Day: " + str(current_day) + " ---")
    print("%1d / %1d Bins are full!" % (num_bins_full,num_bins))

    while run_event.is_set():
        for x in range(0,num_bins):
            if bin_fullness[x] < 100:
                randomval = min_percent_h + (bin_modifier[x] * (max_percent_h-min_percent_h))
                bin_fullness[x] += randomval
                if bin_fullness[x] > 100:
                    bin_fullness[x] = 100
                    num_bins_full += 1
                    print("Bin #"+str(x) + " is full!")

        state['bin_fullness'] = bin_fullness
        state['current_day'] = current_day
        state['current_hour'] = current_hour
        print("Current Hour: " + str(current_hour))
        current_hour += 1
        if current_hour >= day_length:
            current_hour = 0
            current_day += 1
            for x in range(0,num_bins):
                print("Bin #%1d is %2.2f%% full." % (x,bin_fullness[x]))
            print("--- Current Day: " + str(current_day) + " ---")
            print("%1d / %1d Bins are full!" % (num_bins_full,num_bins))
        if test_mode:
            time.sleep(speed/4)
        else:
            time.sleep(speed)

class SimulationHandler(BaseHTTPRequestHandler):
    global num_bins, bin_fullness, num_bins_full, run_event, thread, httpd
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "application/json")
        s.end_headers()
    def do_GET(s):
        global num_bins
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "application/json")
        s.end_headers()
        if "/stop" in s.path:
            run_event.clear()
            thread.join()
            s.wfile.write(("Simulation Stopped").encode())
            httpd.server_close()
            sys.exit()
        if "/setup" in s.path:
            o = urllib.parse.urlparse(s.path)
            queries = urllib.parse.parse_qs(o.query)
            for x in queries.get('num_bins', None):
                num_bins = int(x)
                print("--- Setting Number of Bins to: " + str(num_bins)+ " ---")
                restartThread()
        if s.path == "/status":
            s.wfile.write(json.dumps(state).encode())
        if "/empty" in s.path:
            global bin_fullness
            o = urllib.parse.urlparse(s.path)
            queries = urllib.parse.parse_qs(o.query)
            for x in queries.get('bin', None):
                bin_fullness[int(x)] = 0


def restartThread():
    global run_event, thread
    print("--- Restarting Simulation ---")
    if run_event != None:
        run_event.clear()
    if thread != None:
        thread.join()
    initialize()
    run_event = threading.Event()
    run_event.set()
    thread = threading.Thread(target=simulation, args=("Basic Simulation", run_event))
    thread.start()


def main():
    global run_event, thread, httpd
    initialize()
    run_event = threading.Event()
    run_event.set()

    httpd = HTTPServer((HOST_NAME, PORT_NUMBER), SimulationHandler)
    print (time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        thread = threading.Thread(target=simulation, args=("Basic Simulation", run_event))
        thread.start()
        httpd.serve_forever()

    except KeyboardInterrupt:
        run_event.clear()
        thread.join()

    httpd.server_close()

    print (time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))

if __name__ == '__main__':
    main()
